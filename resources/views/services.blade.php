@extends('layouts.mmac-base')

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">{{trans('titles.services')}}</h1>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3 uk-margin-bottom">
      <section id="what-we-do">
        <hr>
        <h2>{{trans('services.what-we-do.heading')}}</h2>
        {!!trans('services.what-we-do.text')!!}
      </section>
    </div>
  </div>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3 uk-push-1-3 uk-margin-bottom">
      <section id="space-rental">
        <hr>
        <h2>{{trans('services.space-rental.heading')}}</h2>
        {!!trans('services.space-rental.text')!!}
      </section>
    </div>
  </div>

</div>

@stop
