@extends('layouts.mmac-base')

@section('content')


<div class="mmac-container pv">

  <h1 class="mmac-page-title">
    <a href="{{action('ArtistsController@index')}}">{{trans('titles.artists')}}</a>
    <i class="icon-small icon-chevron-small-right"></i>{{$artist->name}}
  </h1>

  <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

    @if(!empty($artist->image))
    <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3">
      <img src="{{asset('images/artists/'.$artist->image)}}" class="uk-width-1-1" alt="" />
    </div>
    @endif

    <div class="uk-width-small-1-1 uk-width-medium-2-3 uk-width-large-2-3">

      <h2>{{trans('artists.biography')}}</h2>

      @if(!empty($artist->biography))
      <p>
        {!!$artist->biography!!}
      </p>
      @endif

      @if(!empty($artist->cv))
      <p>
        <a class="uk-button mmac-button-grey" href="{{asset('cv/'.$artist->cv)}}" target="_blank"> <i class="icon-download"></i> Download CV</a>
      </p>
      @endif

      @if(!empty($artist->website))
      <p>
        <a href="{{$artist->website}}" target="_blank">{{trans('artists.website')}}</a>
      </p>
      @endif

      @if($artist->exhibitions->count()>0)
      <h2>{{trans('artists.exhibitions')}}</h2>
      <ul class="uk-subnav uk-subnav-pill">
        @foreach($artist->exhibitions as $exhibition)
        <li>
          <a href="{{action('ExhibitionsController@show',['id'=>$exhibition->id])}}">
            {{$exhibition->title}}
          </a>
        </li>
        @endforeach
      </ul>
      @endif

    </div>
  </div>
</div>


@if($artist->galleries->count()>0)
  @foreach($artist->galleries as $gallery)
  <div class="uk-width-1-1 pv bggxl">
    <div class="mmac-container">
      <h2>Artist Works</h2>
      <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-3" data-uk-grid={gutter:20}>
        @foreach($gallery->images as $image)
        <div>
          <img src="{{asset('/images/galleries/'.$image->filename)}}" alt="" />
        </div>
        @endforeach
      </div>
    </div>
  </div>
  @endforeach
@endif


@if($artist->items->count()>0)
<div class="uk-width-1-1 pvt__s pvb bggxl">
  <div class="mmac-container">
    <h2>{{trans('artists.works')}}</h2>
    <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
      @foreach($artist->items as $item)
      <div class="uk-width-small-1-2 uk-width-medium-1-3 uk-width-large-1-4">
        <div>
          <a href="{{action('ItemController@show',['id'=>$item->id])}}">
            @if($item->galleries->count()>0)
            <img src="{{asset('images/galleries/t/'.$item->galleries->first()->images->first()->filename)}}" alt="" />
          @else
            <img src="{{asset('images/common/blank/item.jpg')}}" alt="" />
          @endif
            <h3>{{$item->name}}</h3>
          </a>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
@endif

@stop
