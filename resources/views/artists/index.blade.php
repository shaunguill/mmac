@extends('layouts.mmac-base')

@section('addassets')
  <script type="text/javascript" src="{{asset('js/components/grid.min.js')}}"></script>
@stop

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">{{trans('titles.artists')}}</h1>

  @if(count($artists) > 0)

  <div data-uk-grid="{controls:'#artist-control', gutter:18}" class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4">

    @foreach($artists as $artist)

    <div>
      <div>
        <figure class="uk-overlay">
          <a href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}">
          <img src="
          @if(!empty($artist->image))
          {{asset('images/artists/'.$artist->image)}}
          @else
            http://lorempixel.com/400/400
          @endif
          " alt="{{$artist->name}}" />
          </a>
        </figure>
        <div class="uk-panel-box bggd">
          <h3 class="uk-text-contrast uk-margin-remove">
            <a href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}">
              {{$artist->name}}
            </a>
          </h3>
        </div>
      </div>
    </div>

    @endforeach

  </div>

  @else
    <p><mark>No artists found in database</mark></p>
  @endif

</div>

<div class="uk-width-1-1 bggxl" id="artist-of-the-season">
  <div class="mmac-container pv">
    <h1 class="mmac-page-title">Artist of the Season</h1>
    <p class="uk-width-2-3">
      About our program, "the artist of the season" is a space of visibility, promotion and corporate image in which it seeks to generate a global audience open to visual artists working in any medium or discipline, including interdisciplinary artists. The exhibition program is geared towards working artists with a developed portfolio who are intellectually and artistically concerned with pushing boundaries within their disciplines.
    </p>
  </div>
</div>

@stop
