@extends('layouts.mmac-base')
@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">{{trans('art-fairs-events.art-fairs.heading')}}</h1>

  @if(count($fairs) > 0)
    @foreach($fairs as $fair)
    <div class="uk-clearfix">
      <h2>{{$fair->title}}</h2>

      @foreach($fair->galleries as $gallery)
        @include('partials.gallery')
      @endforeach
      <p>{!!$fair->description!!}</p>
    </div>
    <hr>
    @endforeach
  @else
    <p>
      <mark>No fairs found.</mark>
    </p>
  @endif

</div>

<div class="uk-width-1-1 bggxl" id="events">
  <div class="mmac-container pv">
      <h1 class="mmac-page-title">{{ trans('art-fairs-events.events.heading')}}</h1>
      @if(count($events) > 0)
        <div class="uk-grid" data-uk-grid-margin>
          @foreach($events as $event)
            <div class="uk-width-1-1 uk-panel-box bgw uk-clearfix">
              <img src="{{asset('images/events/'.$event->image)}}" class="uk-width-1-3 uk-float-left uk-margin-right uk-margin-bottom" alt="" />
              <h1>{{$event->title}}</h1>
              <p>
                {!! $event->body !!}
              </p>
            </div>
          @endforeach
        </div>
      @else
        <p>
          <mark>No events found.</mark>
        </p>
      @endif
  </div>
</div>

@stop
