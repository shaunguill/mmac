@extends('layouts.mmac-base')

@section('addassets')
  <script type="text/javascript" src="js/components/slideshow.min.js"></script>
  <link rel="stylesheet" href="css/components/slideshow.min.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/components/dotnav.css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="css/components/slidenav.min.css" media="screen" title="no title" charset="utf-8">

  <script type="text/javascript" src="{{asset('js/components/parallax.min.js')}}">

  </script>
@stop


@section('content')

<div class="uk-slidenav-position" data-uk-slideshow="{animation:'scroll',autoplay:true}">
  <ul class="uk-slideshow">

    @foreach(File::allFiles(public_path().'/images/common/covers') as $cover)
    <li>
      <div class="uk-grid">
        <div class="uk-width-1-1" style="background-image:url('images/common/covers/{{$cover->getFilename()}}');background-size:cover;background-position:center;">
          <div class="blip-container uk-text-center uk-vertical-align" style="height:80vh;">
            <div class="uk-vertical-align-bottom" style="padding-bottom:150px">

            </div>
          </div>
        </div>
      </div>
    </li>
    @endforeach

  </ul>
  <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
  <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
  <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
    <!--
    @foreach(File::allFiles(public_path('/images/common/covers')) as $k=>$v)
    <li data-uk-slideshow-item="{{$k}}"><a href="#">Item 1</a></li>
    @endforeach
    -->
  </ul>
  <div class="uk-position-cover uk-flex uk-flex-center uk-flex-middle">
    <div class="uk-text-center intro">
      <img src="images/common/mmac-logo.svg" style="width:300px;height:auto;" alt="" />
      <p>
      <!--  <a href="/en">English</a> | <a href="/es">Espa&ntilde;ol</a> -->
      </p>
    </div>
  </div>
</div>



<!--<div class="uk-width-1-1 uk-flex uk-flex-middle uk-flex-center" style="height:100vh;">
  <div class="uk-width-1-2">

    <p class="uk-text-center" style="font-size:3em;">&middot;</p>
    <hr>
    <h1 class="uk-text-center uk-margin-large-top">Upcoming Events</h1>
    <p class="uk-margin-large-bottom">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </p>
    <hr>
    <p class="uk-text-center" style="font-size:3em;">&middot;</p>

  </div>
</div>-->

<div class="uk-width-1-1 uk-flex uk-flex-middle uk-flex-center" data-uk-parallax="{bg: -200}" style="height:100vh; background-image: url('{{asset('images/common/aots.jpg')}}');">
  <div class="uk-width-5-6">
    <div class="uk-grid uk-grid-divider">
      <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1 uk-text-right">
        <a class="fp-link" href="/art-fairs#events">
            UPCOMING<br/>EVENTS
        </a>
      </div>
      <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
        <a class="fp-link" href="/artists#artist-of-the-season">
          ARTIST OF<br/>THE SEASON
        </a>
      </div>
    </div>

  </div>
</div>




@stop
