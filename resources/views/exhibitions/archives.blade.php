@extends('layouts.mmac-base')

@section('content')

<div class="mmac-container pv">
  <h1 class="mmac-page-title"><a href="{{action('ExhibitionsController@index')}}">
    <i class="icon-small icon-chevron-small-left"></i>{{trans('titles.exhibitions')}}:</a> Archives</h1>
  <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" data-uk-grid="{gutter:20}">
    @foreach($exhibitions as $exhibition)
    <div>
      <figure class="uk-overlay uk-width-1-1">
        <img src="
        @if($exhibition->galleries->first()->images->count()>0)
        {{asset('images/galleries/t/'.$exhibition->galleries->first()->images->first()->filename)}}
        @else
        http://lorempixel.com/400/400
        @endif
        " class="uk-width-1-1" alt="{{$exhibition->title}}">
        <figcaption class="uk-overlay-panel uk-flex uk-flex-bottom">
          <div>
            <h2 class="expotitle">{{$exhibition->title}}</h2>
            <h3 class="expodate">{{$exhibition->startdate->formatLocalized('%d %b %Y')}} &mdash; {{$exhibition->enddate->formatLocalized('%d %b %Y')}}</h3>
            <a href="{{action('ExhibitionsController@show', ['id'=>$exhibition->id])}}" class="uk-position-cover"></a>
          </div>
        </figcaption>
      </figure>
    </div>
    @endforeach
  </div>
</div>

@endsection
