@extends('layouts.mmac-base')
@section('addassets')
  <script src="{{asset('js/components/slideshow.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('css/components/slideshow.min.css')}}" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="{{asset('css/components/slidenav.min.css')}}" media="screen" title="no title" charset="utf-8">

@stop
@section('content')

  <div class="mmac-container pv">
    <h1 class="mmac-page-title"><a href="{{action('ExhibitionsController@index')}}">
      {{trans('titles.exhibitions')}}</a><i class="icon-small icon-chevron-small-right"></i>{{$exhibition->title}}</h1>

    <div class="uk-grid uk-grid-medium">

      <!-- sub-nav -->
      <div class="uk-width-1-4">
        <h3>Data</h3>
        <ul class="uk-list" style="margin-top:0.35em;font-size:0.9em;line-height:1.35em;">
          <li><i class="icon-calendar"></i> {{$exhibition->startdate->format('j M Y')}} &mdash; {{$exhibition->enddate->format('j M Y')}}</li>
          <li><i class="icon-clock"></i> {{$exhibition->openingtimes}}</li>
          <li><i class="icon-ticket"></i> {{$exhibition->price}} &euro;</li>
        </ul>
        <hr>
        @include('exhibitions.subnav')
      </div>

      <!-- content -->
      <div class="uk-width-3-4">
        <!-- images -->
        @if($exhibition->galleries->count() > 0)
        <div class="uk-slidenav-position uk-margin-bottom" data-uk-slideshow>
          <ul class="uk-slideshow">
            @foreach($exhibition->galleries->first()->images as $image)
            <li><img class="uk-margin-small-bottom" src="{{asset('images/galleries/'.$image->filename)}}" alt="" /></li>
            @endforeach
          </ul>
          @if($exhibition->galleries->first()->images()->count() > 1)
          <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
          <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
          @endif
        </div>
        @else
        <div class="uk-alert uk-alert-warning">
          <i class="icon-warning"></i> No images found for this exhibition.
        </div>
        @endif
        <!-- description -->
        <p>
          {!!$exhibition->description!!}
        </p>
        <!-- other information -->
        @if($exhibition->artists->count()>0)
        <h3>{{trans('titles.artists')}}</h3>
        <ul class="uk-subnav uk-subnav-pill">
          @foreach($exhibition->artists as $artist)
          <li>
            <a href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}">
              {{$artist->name}}
            </a>
          </li>
          @endforeach
        </ul>
        @endif

      </div>
    </div>
  </div>

</div>

@stop
