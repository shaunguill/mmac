@extends('layouts.mmac-base')
@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">{{trans('exhibitions.heading')}}</h1>

  <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

    @if($current)
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-medium-1-1">
      <h2><i class="icon-circle-right"></i> {{trans('exhibitions.current.heading')}}</h2>
      <figure class="uk-overlay uk-width-1-1">
        <img src="
        @if($current->galleries->count()>0)
        {{asset('images/galleries/'.$current->galleries->first()->images()->first()->filename)}}
        @else
        {{asset('images/common/blank/exhibition.jpg')}}
        @endif
        " class="uk-width-1-1" alt="">
        <figcaption class="uk-overlay-panel uk-flex uk-flex-bottom">
          <div>
            <h2 class="expotitle">{{$current->title}}</h2>
            <h3 class="expodate">{{$current->startdate->format('j M Y')}} &mdash; {{$current->enddate->format('j M Y')}}</h3>
            <a href="{{action('ExhibitionsController@show', ['id'=>$current->id])}}" class="uk-position-cover"></a>
          </div>
        </figcaption>
      </figure>
    </div>
    @else
    <div class="uk-width-large-1-2 uk-width-small-1-1 uk-width-medium-1-1">
      <h2><mark><i class="icon-warning"></i> No current exhibition found</mark></h2>
    </div>
    @endif

    @if($upcoming)
    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-medium-1-1">
      <h2><i class="icon-circle-up"></i> {{trans('exhibitions.upcoming.heading')}}</h2>
      <figure class="uk-overlay uk-width-1-1">
        <img src="
        @if($upcoming->galleries->count()>0)
        {{asset('images/galleries/'.$upcoming->galleries->first()->images()->first()->filename)}}
        @else
        {{asset('images/common/blank/exhibition.jpg')}}
        @endif
        " class="uk-width-1-1" alt="">
        <figcaption class="uk-overlay-panel uk-flex uk-flex-bottom">
          <div>
            <h2 class="expotitle">{{$upcoming->title}}</h2>
            <h3 class="expodate"> {{$upcoming->startdate->format('j M Y')}} &mdash; {{$upcoming->enddate->format('j M Y')}}</h3>
            <a href="{{action('ExhibitionsController@show', ['id'=>$upcoming->id])}}" class="uk-position-cover"></a>
          </div>
        </figcaption>
      </figure>
    </div>
    @else
    <div class="uk-width-large-1-2 uk-width-small-1-1 uk-width-medium-1-1">
      <h2><mark><i class="icon-warning"></i> No upcoming exhibition found</mark></h2>
    </div>
    @endif

  </div>

  <hr class="uk-margin-large-bottom">

  <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

    <div class="uk-width-1-1">
      <h2 id="archives"><i class="icon-circle-down"></i> {{trans('exhibitions.past.heading')}}</h2>
    </div>

    @foreach($archived as $dustball)

    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
      <figure class="uk-width-1-1 uk-overlay">
        <div class="uk-panel-box bggd">
          <h2 class="expotitle">{{$dustball->title}}</h2>
          <h3 class="expodate">{{$dustball->startdate->format('j M Y')}} &mdash; {{$dustball->enddate->format('j M Y')}}</h3>
        </div>
        <img src="
        @if($dustball->galleries->count()>0)
        {{asset('images/galleries/t/'.$dustball->galleries->first()->images()->first()->filename)}}
        @else
        {{asset('images/common/blank/exhibition.jpg')}}
        @endif
        " style="object-fit:cover;" alt="">
        <figcaption class="uk-overlay-panel uk-flex uk-flex-middle uk-flex-center uk-text-center">
          <div>
            <a href="{{action('ExhibitionsController@show', ['id'=>$dustball->id])}}" class="uk-position-cover"></a>
          </div>
        </figcaption>
      </figure>
    </div>

    @endforeach

    <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
      <figure class="uk-overlay uk-width-1-1">
        <div style="background-color:#323437;width:100%;padding-top:100%">

        </div>
        <figcaption class="uk-overlay-panel uk-flex uk-flex-middle uk-flex-center uk-text-center">
          <div>
            <i class="icon-circle-down"></i>
            <h2 class="expotitle">{{trans('exhibitions.keywords.view-all')}}</h2>
            <h3 class="expodate">{{trans('exhibitions.keywords.archived')}}</h3>
            <a href="exhibitions/archives" class="uk-position-cover"></a>
          </div>
        </figcaption>
      </figure>
    </div>

  </div>

</div>

@stop
