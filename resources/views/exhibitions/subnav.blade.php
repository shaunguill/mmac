@if($upcoming->count()>0)
<div class="uk-panel border-grey-left">
  <h3 class="uk-panel-title">{{trans('titles.upcoming-exhibitions')}}</h3>
  <ul class="uk-list uk-text-small uk-margin-remove">
    @foreach($upcoming as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}"><span class="uk-text-bold" style="font-size:0.8em;">{{$exhibition->startdate->format('m/y')}}</span> {{$exhibition->title}}</a></li>
    @endforeach
  </ul>
</div>
@endif

@if($current->count()>0)
<div class="uk-panel border-grey-left">
  <h3 class="uk-panel-title">{{trans('titles.current-exhibitions')}}</h3>
  <ul class="uk-list uk-text-small uk-margin-remove">
    @foreach($current as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}"><span class="uk-text-bold" style="font-size:0.8em;">{{$exhibition->startdate->format('m/y')}}</span> {{$exhibition->title}}</a></li>
    @endforeach
  </ul>
</div>
@endif

@if($archived->count()>0)
<div class="uk-panel border-grey-left">
  <h3 class="uk-panel-title">{{trans('titles.archived-exhibitions')}}</h3>
  <ul class="uk-list uk-text-small uk-margin-remove">
    @foreach($archived as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}"><span class="uk-text-bold" style="font-size:0.8em;">{{$exhibition->startdate->format('m/y')}}</span> {{$exhibition->title}}</a></li>
    @endforeach
  </ul>
</div>
@endif
