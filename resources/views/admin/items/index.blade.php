@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Shop Items')
@section('content')

<h1 class="mmac-page-title">Shop Items</h1>


@if($items->count()>0)

  @foreach($items as $item)
    {{$item->name}} &mdash; @if(!empty($item->artist->name)) {{$item->artist->name}} @endif
      <a href="{{action('Admin\ItemController@edit', $item->id)}}">edit</a>
       <br>
  @endforeach

@else

  <mark>No items found</mark>

@endif

<hr>

<div class="uk-panel-box">
  <a class="uk-button uk-button-success" href="{{action('Admin\ItemController@create')}}">Create New Item <i class="icon-circle-with-plus"></i></a>
</div>

@endsection
