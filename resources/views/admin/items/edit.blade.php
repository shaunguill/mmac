@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Update Item')
@section('content')

  <h1 class="mmac-page-title">Update Item</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::model($item,['method'=>'PATCH','action'=>['Admin\ItemController@update', $item->id],'class'=>'uk-form uk-form-horizontal']) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="name">Name</label>
    <div class="uk-form-controls">
      {!! Form::text('name', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="price">Price</label>
    <div class="uk-form-controls">
      {!! Form::text('price') !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="artists">Artist</label>
    <div class="uk-form-controls">
        {!! Form::select('artist', $artists, $item->artist_id, ['placeholder'=>'No artist selected']) !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  <div class="uk-form-row">
    {!! Form::hidden('locales[]', $localeCode) !!}
    <label class="uk-form-label" for="description_{{$localeCode}}">Description ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('description_'.$localeCode, $item->translate($localeCode)->description, ['class'=>'editor']) !!}
    </div>
  </div>

  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Update Item', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ItemController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}

  <hr>

  <!-- image gallery for item -->
  <h1 id="images">Image gallery</h1>

  @if($item->galleries->count()>0)
  @foreach($item->galleries as $gallery)

    @include('admin.partials.gallery')

  @endforeach
  @else

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>
      Add Gallery <i class="icon-circle-with-plus"></i>
    </a>
  </div>

  @endif

  <div id="create-gallery" class="uk-modal">
    <div class="uk-modal-dialog">
      <a class="uk-modal-close uk-close"></a>
      {!! Form::open(['class'=>'uk-form','method'=>'POST', 'action'=> ['Admin\ItemController@createGallery', $item->id]]) !!}
      {!! Form::hidden('model', 'App\Item') !!}
      {!! Form::label('title', 'Title') !!}
      {!! Form::text('title') !!}
      {!! Form::submit('Add This Gallery', ['class'=>'uk-button uk-button-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>

  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

@endsection
