@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Item')
@section('content')

  <h1 class="mmac-page-title">Create New Item</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::open(['method'=>'POST','action'=>'Admin\ItemController@store','class'=>'uk-form uk-form-horizontal']) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="name">Name</label>
    <div class="uk-form-controls">
      {!! Form::text('name', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="price">Price</label>
    <div class="uk-form-controls">
      {!! Form::text('price') !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="artists">Artist</label>
    <div class="uk-form-controls">
      {!! Form::select('artist', $artists, null, ['placeholder'=>'No artist selected']) !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  <div class="uk-form-row">
    {!! Form::hidden('locales[]', $localeCode) !!}
    <label class="uk-form-label" for="description_{{$localeCode}}">Description ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('description_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>

  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Add Item', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ItemController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

  @include('admin.partials.errors')

@endsection
