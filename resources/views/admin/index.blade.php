@extends('layouts.admin')
@section('content')

<h1 class="mmac-page-title">Administrative Section</h1>
<h3>
  What do you want to manage?
</h3>
<ul class="uk-subnav uk-subnav-pill uk-text-large">
  <li><a href="{{action('Admin\ExhibitionsController@index')}}"><i class="icon-blackboard"></i> Exhibitions</a></li>
  <li><a href="{{action('Admin\ArtistsController@index')}}">Artists</a></li>
  <li><a href="{{action('Admin\FairController@index')}}">Fairs</a></li>
  <li><a href="{{action('Admin\ArticleController@index')}}">Blog Articles</a></li>
</ul>



@stop
