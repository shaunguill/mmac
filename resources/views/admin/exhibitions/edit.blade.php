@extends('layouts.admin')

@section('addassets')
  <script type="text/javascript" src="{{asset('js/components/grid.min.js')}}"></script>
  <script>
    $.ajaxSetup ({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      },
    });
    </script>
@stop

@section('content')


  <h1 class="mmac-page-title"><a href="{{action('Admin\ExhibitionsController@index')}}">Exhibitions</a>: {{$exhibition->title}}</h1>

    @include('admin.partials.errors')
    @include('admin.partials.messages')

  {!! Form::model($exhibition,['method'=>'PATCH','action'=>['Admin\ExhibitionsController@update', $exhibition->id],'class'=>'uk-form uk-form-horizontal']) !!}

    <div class="uk-form-row">
      <label class="uk-form-label" for="title">Title</label>
      <div class="uk-form-controls">
        {!! Form::text('title', null, ['class'=>'uk-width-1-1']) !!}
      </div>
    </div>


    <div class="uk-form-row">
      <label class="uk-form-label" for="startdate">Start date</label>
      <div class="uk-form-controls">
        <input type="text" id="startdate" name="startdate" data-uk-datepicker="{format:'DD-MM-YYYY'}" value="{{$exhibition->startdate->format('d-m-Y')}}">
      </div>
    </div>

    <div class="uk-form-row">
      <label class="uk-form-label" for="enddate">End date</label>
      <div class="uk-form-controls">
        <input type="text" id="enddate" name="enddate" data-uk-datepicker="{format:'DD-MM-YYYY'}" value="{{$exhibition->enddate->format('d-m-Y')}}">
      </div>
    </div>

    <div class="uk-form-row">
      <label class="uk-form-label" for="location">Location</label>
      <div class="uk-form-controls">
        <input type="text" id="location" name="location" value="{{$exhibition->location}}">
      </div>
    </div>

    <div class="uk-form-row">
      <label class="uk-form-label" for="openingtimes">Times</label>
      <div class="uk-form-controls">
        {!! Form::text('openingtimes') !!}
      </div>
    </div>

    <div class="uk-form-row">
      <label class="uk-form-label" for="price">Price</label>
      <div class="uk-form-controls">
        {!! Form::text('price') !!}
      </div>
    </div>

    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
    <div class="uk-form-row">
      {!! Form::hidden('locales[]', $localeCode) !!}
      <label class="uk-form-label" for="description_{{$localeCode}}">Description ({{$localeCode}})</label>
      <div class="uk-form-controls">
        {!! Form::textarea('description_'.$localeCode, $exhibition->translate($localeCode)->description, ['class'=>'editor']) !!}
      </div>
    </div>
    @endforeach

    <div class="uk-form-row">
      <label class="uk-form-label" for="artist_list">Artists</label>
      <div class="uk-form-controls">
        {!! Form::select('artist_list[]', $artists, null, ['multiple', 'class'=>'uk-width-1-1']) !!}
      </div>
    </div>

    <div class="uk-form-row">
      <label class="uk-form-label"><strong>Actions</strong></label>
      <div class="uk-form-controls">
        {!! Form::submit('Update exhibition', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
        <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ExhibitionsController@index')}}">Cancel</a>
      </div>
    </div>

    {!! Form::close() !!}

    <hr>

    <h1 id="images">Image gallery</h1>

    @if($exhibition->galleries->count()>0)
    @foreach($exhibition->galleries as $gallery)

      @include('admin.partials.gallery')

    @endforeach
    @else

      <div class="uk-panel-box">
        <a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>
          Add Gallery <i class="icon-circle-with-plus"></i>
        </a>
      </div>

    @endif

    <div id="create-gallery" class="uk-modal">
      <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        {!! Form::open(['class'=>'uk-form','method'=>'POST', 'action'=> ['Admin\ExhibitionsController@createGallery', $exhibition->id]]) !!}
        {!! Form::hidden('model', 'App\Exhibition') !!}
        {!! Form::label('title', 'Title') !!}
        {!! Form::text('title') !!}
        {!! Form::submit('Add This Gallery', ['class'=>'uk-button uk-button-success']) !!}
        {!! Form::close() !!}
      </div>
    </div>


<script>
  $(document).ready(function() {
    $('.editor').summernote({
      /*airMode:true,*/
      minHeight:200,

      toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'picture']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']],
      ],

    });
  });
</script>

@stop
