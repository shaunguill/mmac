<div class="subnav">

  <span class="title">{{trans('titles.current-exhibitions')}}</span>
  <ul class="">
    @foreach($current as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}">{{$exhibition->title or 'None found'}}</a></li>
    @endforeach
  </ul>

  <span class="title">{{trans('titles.upcoming-exhibitions')}}</span>
  <ul>
    @foreach($upcoming as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}">{{$exhibition->title or 'None found'}}</a></li>
    @endforeach
  </ul>

  <span class="title">{{trans('titles.archived-exhibitions')}}</span>
  <ul>
    @foreach($archived as $exhibition)
    <li><a href="{{action('ExhibitionsController@show', [$exhibition->id])}}">{{$exhibition->title or 'None found'}}</a></li>
    @endforeach
  </ul>

</div>
