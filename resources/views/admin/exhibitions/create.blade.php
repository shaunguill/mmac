@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Exhibition')
@section('content')

  <h1 class="mmac-page-title">Create new exhibition</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::open(['method'=>'POST','action'=>'Admin\ExhibitionsController@store','class'=>'uk-form uk-form-horizontal']) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="title">Title</label>
    <div class="uk-form-controls">
      {!! Form::text('title', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="startdate">Start date</label>
    <div class="uk-form-controls">
      <input type="text" id="startdate" name="startdate" data-uk-datepicker="{format:'DD-MM-YYYY'}">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="enddate">End date</label>
    <div class="uk-form-controls">
      <input type="text" id="enddate" name="enddate" data-uk-datepicker="{format:'DD-MM-YYYY'}">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="location">Location</label>
    <div class="uk-form-controls">
      <input type="text" id="location" name="location">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="openingtimes">Times</label>
    <div class="uk-form-controls">
      {!! Form::text('openingtimes') !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="price">Price</label>
    <div class="uk-form-controls">
      {!! Form::text('price') !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  <div class="uk-form-row">
    {!! Form::hidden('locales[]', $localeCode) !!}
    <label class="uk-form-label" for="description_{{$localeCode}}">Description ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('description_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label" for="artists">Artists</label>
    <div class="uk-form-controls">
      {!! Form::select('artists[]', $artists, null, ['multiple', 'class' => 'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Create exhibition', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ExhibitionsController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


<script>
  $(document).ready(function() {
    $('.editor').summernote({
      /*airMode:true,*/
      minHeight:200,

      toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'picture']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']],
      ],

    });
  });
</script>

@stop
