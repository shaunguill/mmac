@extends('layouts.admin')

@section('content')

  <h1 class="mmac-page-title">Exhibitions</h1>

  @if($exhibitions->count()>0)

  <div class="uk-grid uk-grid-small" data-uk-grid-margin>

    @foreach($exhibitions as $exhibition)
    <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-2">
      <figure class="uk-overlay">
        <img src="
          @if($exhibition->galleries->count()>0 && $exhibition->galleries->first()->images->count()>0)
            {{asset('images/galleries/'.$exhibition->galleries->first()->images->first()->filename)}}
          @else
            {{asset('images/common/blank/exhibition.jpg')}}
          @endif
        ">
        <figcaption class="uk-overlay-panel uk-overlay-background">
          <h4 class="expotitle">{{$exhibition->title}}</h4>
          <h5 class="expodate">{{$exhibition->startdate->formatLocalized('%d-%m-%Y')}} &ndash; {{$exhibition->enddate->formatLocalized('%d-%m-%Y')}}</h5>

          <ul class="uk-list uk-margin-small-top">
            <li>Artists:
              @if($exhibition->artists->count()>0)
                @foreach($exhibition->artists as $artist)
                  {{$artist->name}}
                @endforeach
              @else
                None
              @endif
            </li>
            <li>Created: {{$exhibition->created_at->diffForHumans()}}</li>
            <li>Updated: {{$exhibition->updated_at->diffForHumans()}}</li>
          </ul>
          <a class="uk-button blip-button-grey uk-button-small" target="_blank" href="{{action('ExhibitionsController@show', ['id'=>$exhibition->id])}}"><i class="uk-icon-eye"></i></a>
          <a class="uk-button uk-button-primary uk-button-small" href="{{ action('Admin\ExhibitionsController@edit', ['id'=>$exhibition->id]) }}"><i class="uk-icon-edit"></i></a>
          <a class="uk-button uk-button-danger uk-button-small" href="#modal" data-uk-modal="{center:true, bgclose:false, target:'#modal-delete-{{ $exhibition->id }}'}"><i class="uk-icon-remove"></i></a>
        </figcaption>
      </figure>
    </div>

    <!-- modal delete-->
    <div id="modal-delete-{{ $exhibition->id }}" class="uk-modal">
      <div class="uk-modal-dialog">
        <a href="" class="uk-modal-close uk-close"></a>
        <h1><i class="uk-icon-item-times"></i> Delete {{ $exhibition->title }}</h1>
        <p>You're about to delete this exhibition, do you want to continue?</p>
        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\ExhibitionsController@destroy', $exhibition->id]]) !!}
        {!! Form::submit('Yes, delete', ['class' => 'uk-button uk-button-danger']) !!}
        <a href="" class="uk-modal-close uk-button">Cancel</a>
        {!! Form::close() !!}
      </div>
    </div>
    @endforeach

  </div>
  @else
    <mark>No exhibitions found</mark>
  @endif

  <hr>

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="{{action('Admin\ExhibitionsController@create')}}">
      Add New Exhibition <i class="icon-circle-with-plus"></i>
    </a>
  </div>

@stop
