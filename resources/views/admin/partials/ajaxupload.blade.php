<script type="text/javascript">

    $(document).ready(function() {
      $('.editor').summernote({
        /*airMode:true,*/
        minHeight:200,

        toolbar: [
          ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['link', 'picture']],
          ['view', ['fullscreen', 'codeview']],
          ['help', ['help']],
        ],

        styleTags: ['p', 'blockquote', 'pre', 'h3', 'h4', 'h5', 'h6'],

        callbacks: {
          onImageUpload: function(files, editor, welEditable) {
            for (var i = files.length -1; i >=0; i--) {
              sendFile(files[i], this);
            }
          }
        },
      });

      function sendFile(file, el) {
        var formdata = new FormData();
        formdata.append('file', file);
        $.ajax({
          data: formdata,
          type: 'POST',
          url: '{{ route($route) }}',
          cache: false,
          contentType: false,
          processData: false,
          success: function(url) {
            alert('Success');
            $(el).summernote('editor.insertImage', url);
          },
          error: function(){
            alert('Error..');
          }
        });
      }
    });
  </script>
