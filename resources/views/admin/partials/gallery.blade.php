<div id="gallery-{{$gallery->id}}" class="uk-panel-box">
  <div class="uk-grid">
    <div class="uk-width-1-1">
      <span><strong>Title: </strong>{{$gallery->title}}</span>
      (<a class="remove-gallery" data-galid="{{$gallery->id}}" href="#">Remove Gallery</a>)
    </div>
    <div class="uk-width-1-2">
      <h2>Add new:</h2>
      <!-- AJAX IMAGE UPLOAD VIA DROPZONE.JS -->
      <link rel="stylesheet" href="{{asset('css/dropzone.css')}}" media="screen" title="no title" charset="utf-8">
      <form action="{{action('Admin\ImageController@dropzoneupload')}}" class="dropzone" id="dropzoneupload">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <input type="hidden" name="gallery_id" value="{{$gallery->id}}">
      </form>
      <!-- END DROPZONE.JS -->
    </div>
    <div class="uk-width-1-2">
      <h2>Current:</h2>
      <div id="gallery_images" class="uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-width-small-1-2" data-uk-observe data-uk-grid="{gutter:10}">
        @foreach($gallery->images as $image)
        <div class="uk-overlay uk-overlay-hover" id="{{$image->id}}">
          <img src="{{asset('/images/galleries/t/'.$image->filename)}}">
          <div class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle uk-text-center">
            <a href="#" class="delete-image uk-button uk-button-danger">
              <i class="icon-cross"></i>
            </a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

<script src="{{asset('js/dropzone.js')}}"></script>
<script src="{{asset('js/gallery-uploads.js')}}"></script>
