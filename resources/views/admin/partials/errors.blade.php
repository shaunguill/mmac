@if ($errors->any())

    @foreach ($errors->all() as $error)
    <div class="uk-alert uk-alert-danger">
      <a href="" class="uk-alert-close uk-close"></a>
      <p>{{$error}}</p>
    </div>
    @endforeach

@endif
