@if(Session::has('status'))
  <div class="uk-alert" data-uk-alert>
    <a href="" class="uk-alert-close uk-close"></a>
    <p>{!!Session::get('status')!!}</p>
  </div>
@endif
