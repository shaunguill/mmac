@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Fair')
@section('content')

  <h1 class="mmac-page-title">Create New Fair</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::open(['method'=>'POST','action'=>'Admin\FairController@store','class'=>'uk-form uk-form-horizontal']) !!}

  <div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="startdate">Start date</label>
    <div class="uk-form-controls">
      <input type="text" data-uk-datepicker="{format:'DD-MM-YYYY'}" id="startdate" name="startdate">
    </div>
  </div>

  <div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="enddate">End date</label>
    <div class="uk-form-controls">
      <input type="text" data-uk-datepicker="{format:'DD-MM-YYYY'}" id="enddate" name="enddate">
    </div>
  </div>

  <hr>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="title_{{$localeCode}}">Title ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::text('title_'.$localeCode, null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('description_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Create exhibition', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ExhibitionsController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


<script>
  $(document).ready(function() {
    $('.editor').summernote({
      /*airMode:true,*/
      minHeight:200,

      toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link', 'picture']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']],
      ],

    });
  });
</script>

@stop
