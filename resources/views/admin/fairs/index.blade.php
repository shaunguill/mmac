@extends('layouts.admin')
@section('content')

  <h1 class="mmac-page-title">Fairs</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  @if($fairs->count()>0)

  <table class="uk-table uk-table-hover uk-table-striped">

    <thead>
      <tr>
        <th>Title (EN/ES)</th>
        <th>Actions</th>
        <th>Galleries?</th>
        <th>Added at</th>
        <th>Updated at</th>
      </tr>
    </thead>

    <tbody>
      @foreach($fairs as $fair)
      <tr>
        <td>
          {{$fair->translate('en')->title}} / {{$fair->translate('es')->title}}
        </td>
        <td>
          <a href="{{ action('Admin\FairController@edit', ['id'=>$fair->id]) }}"><i class="uk-text-primary uk-icon-edit"></i></a>
          <a href="#modal" data-uk-modal="{bgclose:false,target:'#modal-delete-{{ $fair->id }}'}"><i class="uk-text-danger uk-icon-remove"></i></a>
        </td>
        <td>
          @if($fair->galleries->count()>0) <span class="uk-text-success">Yes</span> @else <span class="uk-text-warning">No</span> @endif
        </td>
        <td>
          {{$fair->created_at->diffForHumans()}}
        </td>
        <td>
          {{$fair->updated_at->diffForHumans()}}
        </td>
      </tr>
      <div id="modal-delete-{{ $fair->id }}" class="uk-modal">
        <div class="uk-modal-dialog">
          <a href="" class="uk-modal-close uk-close"></a>
          <h1><i class="uk-icon-item-times"></i> Delete {{ $fair->title }}</h1>
          <p>You're about to delete this fair, do you want to continue?</p>
          {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\FairController@destroy', $fair->id]]) !!}
          {!! Form::submit('Yes, delete', ['class' => 'uk-button uk-button-danger']) !!}
          <a href="" class="uk-modal-close uk-button">Cancel</a>
          {!! Form::close() !!}
        </div>
      </div>
      @endforeach
    </tbody>
  </table>

  @else
    <mark>No fairs found</mark>
  @endif

  <hr>

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="{{action('Admin\FairController@create')}}">
      Add New Fair <i class="icon-circle-with-plus"></i>
    </a>
  </div>

@stop
