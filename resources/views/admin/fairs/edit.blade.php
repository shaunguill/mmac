@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Update Fair')
@section('content')

  <h1 class="mmac-page-title">Update Fair &ldquo;{{$fair->title}}&rdquo;</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::model($fair,['method'=>'PATCH','action'=>['Admin\FairController@update', $fair->id],'class'=>'uk-form uk-form-horizontal']) !!}

  <div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="startdate">Start Date</label>
    <div class="uk-form-controls">
      <input type="text" data-uk-datepicker="{format:'DD-MM-YYYY'}" id="startdate" name="startdate" value="{{$fair->startdate->format('d-m-Y')}}">
    </div>
  </div>

  <div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="enddate">End Date</label>
    <div class="uk-form-controls">
      <input type="text" data-uk-datepicker="{format:'DD-MM-YYYY'}" id="enddate" name="enddate" value="{{$fair->enddate->format('d-m-Y')}}">
    </div>
  </div>

  <hr>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="title_{{$localeCode}}">Title ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::text('title_'.$localeCode, $fair->translate($localeCode)->title, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('description_'.$localeCode, $fair->translate($localeCode)->description, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>

  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Update Fair', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\FairController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}

  <hr>

  <h1 id="images">Image gallery</h1>

  @if($fair->galleries->count()>0)
  @foreach($fair->galleries as $gallery)

    @include('admin.partials.gallery')

  @endforeach
  @else

    <div class="uk-panel-box">
      <a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>
        Add Gallery <i class="icon-circle-with-plus"></i>
      </a>
    </div>

  @endif

  <div id="create-gallery" class="uk-modal">
    <div class="uk-modal-dialog">
      <a class="uk-modal-close uk-close"></a>
      {!! Form::open(['class'=>'uk-form','method'=>'POST', 'action'=> ['Admin\FairController@createGallery', $fair->id]]) !!}
      {!! Form::hidden('model', 'App\Fair') !!}
      {!! Form::label('title', 'Title') !!}
      {!! Form::text('title') !!}
      {!! Form::submit('Add This Gallery', ['class'=>'uk-button uk-button-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>

  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

@endsection
