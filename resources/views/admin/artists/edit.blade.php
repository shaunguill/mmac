@extends('layouts.admin')

@section('content')


  <h1 class="mmac-page-title">Update Artist: {{$artist->name}}</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::model($artist,['method'=>'PATCH','action'=>['Admin\ArtistsController@update', $artist->slug],'class'=>'uk-form uk-form-horizontal', 'files' => 'true']) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="name">Name</label>
    <div class="uk-form-controls">
      {!! Form::text('name', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>


  <div class="uk-form-row">
    <label class="uk-form-label" for="image">Image</label>
    @if($artist->image)
    <img src="{{asset('images/artists/'.$artist->image)}}" class="uk-width-1-3 uk-margin-left uk-margin-right uk-float-left" alt="" />
    <div class="btn-group" data-toggle="buttons-checkbox">
        <a class="btn btn-default collapse-data-btn" data-toggle="collapse" href="#details">Change This Image</a>
    </div>
    <div id="details" class="form-group collapse">
      {!! Form::label('image', 'Select image') !!}
      {!! Form::file('image_new', null, ['class'=>'form-control']) !!}
    </div>
    @else
      {!! Form::file('image', null, ['class'=>'form-control']) !!}
    @endif
  </div>


  <div class="uk-form-row">
    @if(!empty($artist->cv))
    <label class="uk-form-label" for="cv"><span class="uk-text-primary">Update</span> CV</label>
    <div class="uk-form-controls">
      {!! Form::file('cv_new', null, ['class'=>'form-control']) !!}
    </div>
    @else
    <label class="uk-form-label" for="cv_new"><span class="uk-text-danger">No CV set!</span> Upload one?</label>
    <div class="uk-form-controls">
      {!! Form::file('cv', null, ['class'=>'form-control']) !!}
    </div>
    @endif
  </div>

  @if(!empty($artist->cv))
  <div class="uk-form-row">
    <label class="uk-form-label" for="name"><span class="uk-text-warning">Remove</span> CV</label>
    <div class="uk-form-controls">
      {!! Form::checkbox('remove_cv') !!} <span>Check this box if you want to remove the CV</span>
    </div>
  </div>
  @endif

  <div class="uk-form-row">
    <label class="uk-form-label" for="website">Website</label>
    <div class="uk-form-controls">
      {!! Form::text('website', null, ['class'=>'uk-width-1-1', 'placeholder'=>'http://www.website.com']) !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  <div class="uk-form-row">
    {!! Form::hidden('locales[]', $localeCode) !!}
    <label class="uk-form-label" for="biography_{{$localeCode}}">Biography ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('biography_'.$localeCode, $artist->translate($localeCode)->biography, ['class'=>'editor']) !!}
    </div>
  </div>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label" for="exhibition_list">Exhibitions</label>
    <div class="uk-form-controls">
      {!! Form::select('exhibition_list[]', $exhibitions, null, ['multiple', 'class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Update Artist', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ArtistsController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}



<!--galleries-->

<h1 id="images">Image gallery</h1>

@if($artist->galleries->count()>0)
@foreach($artist->galleries as $gallery)

  @include('admin.partials.gallery')

@endforeach
@else

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>
      Add Gallery <i class="icon-circle-with-plus"></i>
    </a>
  </div>

@endif

<div id="create-gallery" class="uk-modal">
  <div class="uk-modal-dialog">
    <a class="uk-modal-close uk-close"></a>
    {!! Form::open(['class'=>'uk-form','method'=>'POST', 'action'=> ['Admin\ArtistsController@createGallery', $artist->id]]) !!}
    {!! Form::hidden('model', 'App\Artist') !!}
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title') !!}
    {!! Form::submit('Add This Gallery', ['class'=>'uk-button uk-button-success']) !!}
    {!! Form::close() !!}
  </div>
</div>


  <script>
    $(document).ready(function() {
      $('.editor').summernote({
        /*airMode:true,*/
        minHeight:200,

        toolbar: [
          ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['insert', ['link']],
          ['view', ['fullscreen', 'codeview']],
          ['help', ['help']],
        ],
      });
    });
  </script>


@stop
