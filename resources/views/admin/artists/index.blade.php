@extends('layouts.admin')

@section('content')



  <h1 class="mmac-page-title">Artists</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  @if($artists->count()>0)

  <table class="uk-table uk-table-hover uk-table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Actions</th>
        <th>Exhibitions</th>
        <th>CV</th>
        <th>Created at</th>
        <th>Updated at</th>
      </tr>
    </thead>
    <tbody>
      @foreach($artists as $artist)
      <tr>
        <td>
          {{$artist->name}}
        </td>
        <td>
          <a target="_blank" href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}"><i class="uk-icon-eye"></i></a>
          <a href="{{ action('Admin\ArtistsController@edit', ['slug'=>$artist->slug]) }}"><i class="uk-icon-edit uk-text-primary"></i></a>
          <a href="#modal" data-uk-modal="{bgclose:false,target:'#modal-delete-{{ $artist->id }}'}"><i class="uk-icon-remove uk-text-danger"></i></a>
          <div id="modal-delete-{{ $artist->id }}" class="uk-modal">
            <div class="uk-modal-dialog">
              <a href="" class="uk-modal-close uk-close"></a>
              <h1><i class="uk-icon-item-times"></i> Delete {{ $artist->name }}</h1>
              <p>You're about to delete this artist, do you want to continue?</p>
              {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\ArtistsController@destroy', $artist->id]]) !!}
              {!! Form::submit('Yes, delete', ['class' => 'uk-button uk-button-danger']) !!}
              <a href="" class="uk-modal-close uk-button">Cancel</a>
              {!! Form::close() !!}
            </div>
          </div>
        </td>
        <td>
          {{$artist->exhibitions()->count()}}
        </td>
        <td>
          @if(!empty($artist->cv)) <span class="uk-text-success">Yes</span> @else <span class="uk-text-warning">No</span> @endif
        </td>
        <td>
          {{$artist->created_at->diffForHumans()}}
        </td>
        <td>
          {{$artist->updated_at->diffForHumans()}}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  @else
    <mark>No artists found</mark>
  @endif

  <hr>

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="{{action('Admin\ArtistsController@create')}}">Add New Artist <i class="icon-circle-with-plus"></i></a>
  </div>

@stop
