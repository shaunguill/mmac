@extends('layouts.admin')

@section('content')


  <h1 class="mmac-page-title">Create New Artist</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  {!! Form::open(['action'=>'Admin\ArtistsController@store','class'=>'uk-form uk-form-horizontal', 'files'=>'true']) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="name">Name</label>
    <div class="uk-form-controls">
      {!! Form::text('name', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="image">Image</label>
    <div class="uk-form-controls">
      {!! Form::file('image', null, ['class'=>'form-control']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="image">CV (PDF)</label>
    <div class="uk-form-controls">
      {!! Form::file('cv', null, ['class'=>'form-control']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="website">Website</label>
    <div class="uk-form-controls">
      {!! Form::text('website', null, ['class'=>'uk-width-1-1', 'placeholder'=>'http://www.website.com']) !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  <div class="uk-form-row">
    {!! Form::hidden('locales[]', $localeCode) !!}
    <label class="uk-form-label" for="biography_{{$localeCode}}">Biography ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('biography_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label" for="exhibitions">Exhibitions</label>
    <div class="uk-form-controls">
      {!! Form::select('exhibitions[]', $exhibitions, null, ['multiple', 'class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Create Artist', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\ArtistsController@index')}}">Cancel</a>
    </div>
  </div>
  {!! Form::close() !!}


<script>
  $(document).ready(function() {
    $('.editor').summernote({
      /*airMode:true,*/
      minHeight:200,

      toolbar: [
        ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['insert', ['link']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']],
      ],
    });
  });
</script>

@stop
