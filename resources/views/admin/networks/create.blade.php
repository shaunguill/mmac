@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Article')
@section('content')

  <h1 class="mmac-page-title">Create New International Network</h1>

  {!! Form::open(['method'=>'POST','action'=>'Admin\NetworkController@store','class'=>'uk-form uk-form-horizontal']) !!}

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="title_{{$localeCode}}">Title ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::text('title_'.$localeCode, null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('body_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Add Network', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\NetworkController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

  @include('admin.partials.errors')

@endsection
