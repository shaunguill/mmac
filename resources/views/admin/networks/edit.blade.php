@extends('layouts.admin')
@section('content')

  <h1 class="mmac-page-title">Update Network</h1>

    @include('admin.partials.errors')
    @include('admin.partials.messages')

  {!! Form::model($network,['method'=>'PATCH','action'=>['Admin\NetworkController@update', $network->id],'class'=>'uk-form uk-form-horizontal']) !!}

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="title_{{$localeCode}}">Title ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::text('title_'.$localeCode, $network->translate($localeCode)->title, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('body_'.$localeCode, $network->translate($localeCode)->body, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>

  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Update Network', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\NetworkController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}

  <hr>

  <h1 id="images">Image gallery</h1>

  @if($network->galleries->count()>0)
  @foreach($network->galleries as $gallery)

    @include('admin.partials.gallery')

  @endforeach
  @else

    <div class="uk-panel-box">
      <a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>
        Add Gallery <i class="icon-circle-with-plus"></i>
      </a>
    </div>

  @endif

  <div id="create-gallery" class="uk-modal">
    <div class="uk-modal-dialog">
      <a class="uk-modal-close uk-close"></a>
      {!! Form::open(['class'=>'uk-form','method'=>'POST', 'action'=> ['Admin\NetworkController@createGallery', $network->id]]) !!}
      {!! Form::hidden('model', 'App\Network') !!}
      {!! Form::label('title', 'Title') !!}
      {!! Form::text('title') !!}
      {!! Form::submit('Add This Gallery', ['class'=>'uk-button uk-button-success']) !!}
      {!! Form::close() !!}
    </div>
  </div>

  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

@endsection
