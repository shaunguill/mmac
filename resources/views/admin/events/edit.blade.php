@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Article')
@section('content')

  <h1 class="mmac-page-title">Edit New Event &ldquo;{{$event->title}}&rdquo;</h1>

  {!! Form::model($event,['method'=>'PATCH','action'=>['Admin\EventController@update', $event->id],'class'=>'uk-form uk-form-horizontal', 'files'=>true]) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="title">Title</label>
    <div class="uk-form-controls">
      {!! Form::text('title', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="startdate">Start date</label>
    <div class="uk-form-controls">
      <input type="text" id="startdate" name="startdate" data-uk-datepicker="{format:'DD-MM-YYYY'}" value="{{$event->startdate->format('d-m-Y')}}">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="enddate">End date</label>
    <div class="uk-form-controls">
      <input type="text" id="enddate" name="enddate" data-uk-datepicker="{format:'DD-MM-YYYY'}" value="{{$event->enddate->format('d-m-Y')}}">
    </div>
  </div>

  <div class="uk-form-row uk-margin-bottom">
    <label class="uk-form-label" for="image">Image</label>
    @if($event->image)
    <img src="{{asset('images/events/'.$event->image)}}" class="uk-width-1-3 uk-margin-left uk-margin-right uk-float-left" alt="" />
    <div class="btn-group" data-toggle="buttons-checkbox">
      <a class="btn btn-default collapse-data-btn" data-toggle="collapse" href="#details">Change This Image</a>
    </div>
    <div id="details" class="form-group collapse">
      {!! Form::label('image', 'Select image') !!}
      {!! Form::file('image_new', null, ['class'=>'form-control']) !!}
    </div>
    @else
      {!! Form::file('image', null, ['class'=>'form-control']) !!}
    @endif
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('body_'.$localeCode, $event->translate($localeCode)->body, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Add Event', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\EventController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

  @include('admin.partials.errors')

@endsection
