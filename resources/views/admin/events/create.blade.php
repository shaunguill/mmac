@extends('layouts.admin')
@section('title', 'MMAC ADMIN - Create New Article')
@section('content')

  <h1 class="mmac-page-title">Create New Event</h1>

  {!! Form::open(['method'=>'POST','action'=>'Admin\EventController@store','class'=>'uk-form uk-form-horizontal', 'files'=>true]) !!}

  <div class="uk-form-row">
    <label class="uk-form-label" for="title">Title</label>
    <div class="uk-form-controls">
      {!! Form::text('title', null, ['class'=>'uk-width-1-1']) !!}
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="startdate">Start date</label>
    <div class="uk-form-controls">
      <input type="text" id="startdate" name="startdate" data-uk-datepicker="{format:'DD-MM-YYYY'}">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="enddate">End date</label>
    <div class="uk-form-controls">
      <input type="text" id="enddate" name="enddate" data-uk-datepicker="{format:'DD-MM-YYYY'}">
    </div>
  </div>

  <div class="uk-form-row">
    <label class="uk-form-label" for="image">Image</label>
    <div class="uk-form-controls">
      {!! Form::file('image', null, ['class'=>'form-control']) !!}
    </div>
  </div>

  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
  {!! Form::hidden('locales[]', $localeCode) !!}
  <div class="uk-form-row">
    <label class="uk-form-label" for="body_{{$localeCode}}">Body text ({{$localeCode}})</label>
    <div class="uk-form-controls">
      {!! Form::textarea('body_'.$localeCode, null, ['class'=>'editor']) !!}
    </div>
  </div>

  <hr>
  @endforeach

  <div class="uk-form-row">
    <label class="uk-form-label"><strong>Actions</strong></label>
    <div class="uk-form-controls">
      {!! Form::submit('Add Event', ['class'=>'uk-button uk-button-large uk-button-primary']) !!}
      <a class="uk-button uk-button-large uk-button-danger" href="{{action('Admin\EventController@index')}}">Cancel</a>
    </div>
  </div>

  {!! Form::close() !!}


  @include('admin.partials.ajaxupload', ['route'=>'ajaxupload'])

  @include('admin.partials.errors')

@endsection
