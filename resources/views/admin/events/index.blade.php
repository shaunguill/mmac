@extends('layouts.admin')
@section('content')

  <h1 class="mmac-page-title">Events</h1>

  @if($events->count()>0)

    <table class="uk-table uk-table-hover uk-table-striped">
      <thead>
        <tr>
          <th>Title</th>
          <th>Actions</th>
          <th>Galleries?</th>
          <th>Created at</th>
          <th>Updated at</th>
        </tr>
      </thead>
      <tbody>
        @foreach($events as $event)
        <tr>
          <td>
            {{$event->title}}
          </td>
          <td>
            <a href="{{action('Admin\EventController@edit', ['id'=>$event->id])}}"><i class="uk-text-primary icon-edit"></i></a>
            <a href="#modal-delete-{{ $event->id }}" data-uk-modal="{bgclose:false}"><i class="uk-text-danger icon-cross"></i></a>
          </td>
          <td>
            @if($event->galleries->count()>0) <span class="uk-text-success">Yes</span> @else <span class="uk-text-warning">No</span> @endif
          </td>
          <td>
            {{$event->created_at}}
          </td>
          <td>
            {{$event->updated_at}}
          </td>
        </tr>
        <div id="modal-delete-{{ $event->id }}" class="uk-modal">
          <div class="uk-modal-dialog">
            <a href="" class="uk-modal-close uk-close"></a>
            <h1><i class="uk-icon-item-times"></i> Delete {{ $event->title }}</h1>
            <p>You're about to delete this event, do you want to continue?</p>
            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\EventController@destroy', $event->id]]) !!}
            {!! Form::submit('Yes, delete', ['class' => 'uk-button uk-button-danger']) !!}
            <a href="" class="uk-modal-close uk-button">Cancel</a>
            {!! Form::close() !!}
          </div>
        </div>
        @endforeach
      </tbody>
    </table>

  @else
    <mark>No Events</mark>
  @endif

  <hr>

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="{{action('Admin\EventController@create')}}">
      Add New Event <i class="icon-circle-with-plus"></i>
    </a>
  </div>

@endsection
