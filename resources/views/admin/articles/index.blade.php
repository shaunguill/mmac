@extends('layouts.admin')
@section('content')

  <h1 class="mmac-page-title">Blog articles</h1>

  @include('admin.partials.errors')
  @include('admin.partials.messages')

  @if($articles->count()>0)

  <table class="uk-table uk-table-hover uk-table-striped">

    <thead>
      <tr>
        <th>Title (EN/ES)</th>
        <th>Actions</th>
        <th>Galleries?</th>
        <th>Added at</th>
        <th>Updated at</th>
      </tr>
    </thead>

    <tbody>
      @foreach($articles as $article)
      <tr>
        <td>
          {{$article->translate('en')->title}} / {{$article->translate('es')->title}}
        </td>
        <td>
          <a href="{{ action('Admin\ArticleController@edit', ['id'=>$article->id]) }}"><i class="uk-text-primary icon-edit"></i></a>
          <a href="#modal" data-uk-modal="{bgclose:false,target:'#modal-delete-{{ $article->id }}'}"><i class="uk-text-danger icon-cross"></i></a>
        </td>
        <td>
          @if($article->galleries->count()>0) <span class="uk-text-success">Yes</span> @else <span class="uk-text-warning">No</span> @endif
        </td>
        <td>
          {{$article->created_at->diffForHumans()}}
        </td>
        <td>
          {{$article->updated_at->diffForHumans()}}
        </td>
      </tr>
      <div id="modal-delete-{{ $article->id }}" class="uk-modal">
        <div class="uk-modal-dialog">
          <a href="" class="uk-modal-close uk-close"></a>
          <h1><i class="uk-icon-item-times"></i> Delete {{ $article->title }}</h1>
          <p>You're about to delete this article, do you want to continue?</p>
          {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\ArticleController@destroy', $article->id]]) !!}
          {!! Form::submit('Yes, delete', ['class' => 'uk-button uk-button-danger']) !!}
          <a href="" class="uk-modal-close uk-button">Cancel</a>
          {!! Form::close() !!}
        </div>
      </div>
      @endforeach
    </tbody>
  </table>

  @else
    <mark>No articles found</mark>
  @endif

  <hr>

  <div class="uk-panel-box">
    <a class="uk-button uk-button-success" href="{{action('Admin\ArticleController@create')}}">
      Add New Article <i class="icon-circle-with-plus"></i>
    </a>
  </div>

@stop
