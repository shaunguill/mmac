@extends('layouts.basev2')
@section('content')

  <h1>{{trans('art-fairs-events.art-fairs.heading')}}</h1>

  @if(count($fairs) > 0)

    @foreach($fairs as $fair)
    <div class="uk-grid">
      <div class="@if($fair->galleries->count()>0) uk-width-1-2 @else uk-width-2-3 @endif ">
        <h2 style="border-left: solid 2px #e3e3e3; padding-left: 0.5em;" class="uk-margin-small-bottom">{{$fair->title}} </h2>
        <p class="uk-text-bold uk-margin-remove">
          <i class="icon-calendar"></i> {{$fair->startdate->format('j M Y')}} &ndash; {{$fair->enddate->format('j M Y')}}
        </p>
        <p>{!!$fair->description!!}</p>
      </div>
      @if($fair->galleries->count()>0)
      <div class="uk-width-1-2">
        @foreach($fair->galleries as $gallery)
        <div class="uk-panel-box">
          <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-3" data-uk-grid="{gutter:2}">
            @foreach($gallery->images as $image)
            <div>
              <a data-uk-lightbox="{group:'gal-{{$gallery->id}}'}" title="{{$image->description}}" href="{{asset('images/galleries/'.$image->filename)}}">
                <img class="uk-width-1-1" src="{{asset('images/galleries/t/'.$image->filename)}}" alt="" />
              </a>
            </div>
            @endforeach
          </div>
        </div>
        @endforeach
      </div>
      @endif
    </div>
    <hr>
    @endforeach

  @endif


<div class="uk-width-1-1 bggxl" id="events">
  <div class="mmac-container pv">
      <h1 class="mmac-page-title">{{ trans('art-fairs-events.events.heading')}}</h1>
      @if(count($events) > 0)
        <div class="uk-grid" data-uk-grid-margin>
          @foreach($events as $event)
            <div class="uk-width-1-1 uk-panel-box bgw uk-clearfix">
              <img src="{{asset('images/events/'.$event->image)}}" class="uk-width-1-3 uk-float-left uk-margin-right uk-margin-bottom" alt="" />
              <h1>{{$event->title}}</h1>
              <p>
                {!! $event->body !!}
              </p>
            </div>
          @endforeach
        </div>
      @else
        <p>
          <mark>No events found.</mark>
        </p>
      @endif
  </div>
</div>

@endsection
