@extends('layouts.basev2')
@section('content')

  <h1>{{trans('titles.services')}}</h1>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3 uk-margin-bottom">
      <section id="what-we-do">
        <hr>
        <h2>{{trans('services.what-we-do.heading')}}</h2>
        {!!trans('services.what-we-do.text')!!}
      </section>
    </div>
  </div>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3 uk-push-1-3 uk-margin-bottom">
      <section id="space-rental">
        <hr>
        <h2>{{trans('services.space-rental.heading')}}</h2>
        {!!trans('services.space-rental.text')!!}
      </section>
    </div>
  </div>

@endsection
