@extends('layouts.basev2')
@section('content')

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-2-3">
      <article class="uk-article">

        <h1 class="uk-article-title uk-margin-small-bottom">{{$exhibition->title}}</h1>

        <ul class="uk-subnav uk-subnav-pill uk-subnav-line uk-article-meta">
          <li><p><i class="c-b uk-icon-calendar"></i> {{$exhibition->startdate->format('j M Y')}} &ndash; {{$exhibition->enddate->format('j M Y')}}</p></li>
          <li><p><i class="c-b uk-icon-clock-o"></i> {{$exhibition->openingtimes}}</p></li>
          @if($exhibition->price > 0)
          <li><p><i class="c-b uk-icon-ticket"></i> {{$exhibition->price}} &euro;</p></li>
          @endif
          @if(!empty($exhibition->location))
          <li><p><i style="color:#000;" class="uk-icon-map-marker"></i> {{$exhibition->location}}</p></li>
          @endif
        </ul>

        @if($exhibition->galleries->count()>0)
          @foreach($exhibition->galleries->first()->images as $image)
          <img class="uk-width-1-1" src="{{asset('images/galleries/'.$image->filename)}}" />
          @endforeach
        @endif

        <div class="uk-panel-box descriptor border-grey-bottom">
          @if($exhibition->artists->count()>0)
          <h3>{{trans('titles.artists')}}</h3>
            @foreach($exhibition->artists as $artist)
            <a class="uk-button uk-button-small" href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}">
              <i class="uk-icon-dot-circle-o c-b"></i> {{$artist->name}}
            </a>
            @endforeach
          @endif
          <h3>Description</h3>
          {!!$exhibition->description!!}
        </div>

      </article>
    </div>

    <div class="uk-width-medium-1-3">
      @include('exhibitions.subnav')
    </div>

  </div>

@endsection
