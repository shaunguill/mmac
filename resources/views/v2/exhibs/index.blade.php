@extends('layouts.basev2')
@section('content')

<h1>{{trans('exhibitions.heading')}}</h1>

<div id="exhib-control" class="uk-margin-bottom">
  <div class="uk-button-dropdown" data-uk-dropdown="{hoverDelayIdle: 50, remaintime: 50}">
    <button type="button" class="uk-button" name="button">Sort by Date <i class="uk-icon-chevron-down"></i></button>
    <div class="uk-dropdown">
      <ul class="uk-nav uk-nav-dropdown">
        <li data-uk-sort="item-date"><a href="">Ascending</a></li>
        <li data-uk-sort="item-date:desc"><a href="">Descending</a></li>
      </ul>
    </div>
  </div>
  <div class="uk-button-dropdown" data-uk-dropdown="{hoverDelayIdle: 50, remaintime: 50}">
    <button type="button" class="uk-button" name="button">Sort by Title <i class="uk-icon-chevron-down"></i></button>
    <div class="uk-dropdown">
      <ul class="uk-nav uk-nav-dropdown">
        <li data-uk-sort="item-name"><a href="">Ascending</a></li>
        <li data-uk-sort="item-name:desc"><a href="">Descending</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="{controls:'#exhib-control',gutter:40}">
  @foreach($archived as $e)
  <div data-item-date="{{$e->enddate}}" data-item-name="{{$e->title}}">
    <figure class="uk-width-1-1 uk-overlay uk-overlay-hover">
      <img src="
      @if($e->galleries->count()>0)
      {{asset('images/galleries/t/'.$e->galleries->first()->images()->first()->filename)}}
      @else
      {{asset('images/common/blank/exhibition.jpg')}}
      @endif
      " style="object-fit:cover;" class="uk-width-1-1" alt="">
      <figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-middle uk-flex-center uk-text-center">
        <div>
          <i class="uk-icon-plus"></i>
        </div>
      </figcaption>
      <a href="{{action('ExhibitionsController@show', ['id'=>$e->id])}}" class="uk-position-cover"></a>
    </figure>
    <div class="uk-panel-box p-s border-grey-bottom">
      <h3 class="uk-margin-remove">{{$e->title}}</h3>
      <h5 class="uk-margin-remove">{{$e->startdate->format('j M Y')}} &mdash; {{$e->enddate->format('j M Y')}}</h5>
    </div>
  </div>
  @endforeach
</div>

@endsection
