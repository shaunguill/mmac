@extends('layouts.basev2')
@section('content')

  <h1>
    <a href="{{action('ArtistsController@index')}}">{{trans('titles.artists')}}:</a>
    <i class="icon-small icon-chevron-small-right"></i>{{$artist->name}}
  </h1>

  <div class="uk-grid" data-uk-grid-margin>

    @if(!empty($artist->image))
    <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3">
      <img src="{{asset('images/artists/'.$artist->image)}}" class="uk-width-1-1" alt="" />
    </div>
    @endif

    <div class="uk-width-small-1-1 uk-width-medium-2-3 uk-width-large-2-3">

      <h2>{{trans('artists.biography')}}</h2>

      @if(!empty($artist->biography))
      <p>
        {!!$artist->biography!!}
      </p>
      @endif

      @if(!empty($artist->cv))
        <a class="uk-button" href="{{asset('cv/'.$artist->cv)}}" target="_blank"> <i class="icon-download"></i> @lang('artists.download-cv')</a>
      @endif

      @if(!empty($artist->website))
        <a class="uk-button" href="{{$artist->website}}" target="_blank">@lang('artists.website')</a>
      @endif

      @if($artist->exhibitions->count()>0)
      <h2>{{trans('artists.exhibitions')}}</h2>

        @foreach($artist->exhibitions as $exhibition)
        <a class="uk-button uk-button-small" href="{{action('ExhibitionsController@show',['id'=>$exhibition->id])}}">
          <i class="uk-icon-dot-circle-o c-b"></i> {{$exhibition->title}}
        </a>
        @endforeach

      @endif

    </div>
  </div>

  @if($artist->galleries->count()>0)
    <hr>
  @foreach($artist->galleries as $gallery)
  <div class="uk-width-1-1">
    <h2>@lang('artists.gallery')</h2>
    <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" data-uk-grid={gutter:40}>
      @foreach($gallery->images as $image)
        <div>
          <figure class="uk-overlay uk-overlay-hover">
            <img src="{{asset('/images/galleries/'.$image->filename)}}" class="uk-width-1-1" />
            <figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center">
              <div>@lang('common.view')</div>
            </figcaption>
            <a class="uk-position-cover" href="#"></a>
          </figure>
        </div>
      @endforeach
    </div>
  </div>
  @endforeach
  @endif


@endsection
