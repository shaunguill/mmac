@if($artist->items->count()>0)
<div class="uk-width-1-1">

  <h2>{{trans('artists.works')}}</h2>
  <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
    @foreach($artist->items as $item)
    <div class="uk-width-small-1-2 uk-width-medium-1-3 uk-width-large-1-4">
      <div>
        <a href="{{action('ItemController@show',['id'=>$item->id])}}">
          @if($item->galleries->count()>0)
          <img src="{{asset('images/galleries/t/'.$item->galleries->first()->images->first()->filename)}}" class="uk-width-1-1" />
        @else
          <img src="{{asset('images/common/blank/item.jpg')}}" class="uk-width-1-1" />
        @endif
          <h3>{{$item->name}}</h3>
        </a>
      </div>
    </div>
    @endforeach
  </div>

</div>
@endif
