@extends('layouts.basev2')
@section('content')

  <h1>{{trans('titles.artists')}} index</h1>

  <div data-uk-grid="{gutter:40}" class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4">

    @foreach($artists as $artist)

    <div>
      <div>
        <figure class="uk-overlay">
          <a href="{{action('ArtistsController@show', ['slug'=>$artist->slug])}}">
          <img src="
          @if(!empty($artist->image))
          {{asset('images/artists/'.$artist->image)}}
          @else
            http://lorempixel.com/400/400
          @endif
          " alt="{{$artist->name}}" />
          </a>
        </figure>
        <div class="uk-panel-box p-s border-grey-bottom">
          <h4 class="uk-margin-remove">
            {{$artist->name}}
          </h4>
        </div>
      </div>
    </div>

    @endforeach

  </div>

@endsection
