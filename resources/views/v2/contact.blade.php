@extends('layouts.basev2')
@section('content')

<h1 class="mmac-page-title">{{ trans('contact.heading') }}</h1>

<div class="uk-grid" data-uk-grid-margin>

  <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-1">

    <p class="uk-text-large uk-margin-remove">
      {{trans('contact.text.mmac-gallery')}}
    </p>
    <p class="uk-text-large uk-margin-remove">
      Mamacuchara
    </p>
    <h4>
      {{trans('contact.text.space')}}
    </h4>
    <ul class="uk-list uk-list-space">
      <li>13 Carabassa</li>
      <li>Barcelona, {{trans('contact.text.spain')}} 08002</li>
      <li><i class="uk-icon-mobile uk-icon-justify"></i> (0034) 654623602</li>
      <li><i class="uk-icon-envelope uk-icon-justify"></i> <a href="mailto:info@mmacgallery.com"> info@mmacgallery.com </a></li>
    </ul>
  </div>

  <div class="uk-width-large-2-3 uk-width-medium-2-3 uk-width-small-1-1">
    <div id="map-canvas" style="width:100%; height:350px;top:0px;"></div>
  </div>


  <div class="uk-width-1-1 uk-text-center">
    <div class="uk-panel-box">
      <h1>{{trans('contact.text.connect-online')}}</h1>
      <ul class="uk-subnav uk-flex uk-flex-center">
        <li><a target="_blank" href="https://www.facebook.com/mmacss"><i class="uk-icon-large icon-hover uk-icon-facebook-official"></i></a></li>
        <li><a target="_blank" href="https://www.linkedin.com/in/mmac-mamacuchara-a6965898"><i class="uk-icon-large icon-hover uk-icon-linkedin"></i></a></li>
        <li><a target="_blank" href="https://twitter.com/MMACgallery"><i class="uk-icon-large icon-hover uk-icon-twitter"></i></a></li>
        <li><a target="_blank" href="https://www.instagram.com/mmacgallery/"><i class="uk-icon-large icon-hover uk-icon-instagram"></i></a></li>
        <li><a target="_blank" href="https://www.youtube.com/user/mmacgallery"><i class="uk-icon-large icon-hover uk-icon-youtube"></i></a></li>
        <li><a target="_blank" href="https://vimeo.com/gallerymmac"><i class="uk-icon-large icon-hover uk-icon-vimeo-square"></i></a></li>
      </ul>
    </div>
  </div>

  <div class="uk-width-1-1">
    <div class="uk-flex uk-flex-middle uk-flex-center" style="min-height:300px; background-image:url('../images/common/contact-hours.jpg');background-size:cover;background-position:right top">
      <div>
        <h1 class="uk-text-center">{{trans('contact.text.hours')}}</h1>
        <div class="uk-grid uk-grid-small">
          <div class="uk-width-1-2 uk-text-right">
            <ul class="uk-list uk-list-space">
              <li><span class="uk-text-bold">{{trans('contact.text.tue-fri')}}</span></li>
              <li><span class="uk-text-bold">{{trans('contact.text.sat')}}</span></li>
              <li><span class="uk-text-bold">{{trans('contact.text.holidays')}}</span></li>
              <li><span class="uk-text-bold">{{trans('contact.text.sunday')}}</span></li>
            </ul>
          </div>
          <div class="uk-width-1-2">
            <ul class="uk-list uk-list-space">
              <li> 16 am &ndash; 8.30 pm</li>
              <li> 14 am &ndash; 7 pm</li>
              <li> 14 am &ndash; 18 pm</li>
              <li> &mdash;</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>







<script src="https://maps.googleapis.com/maps/api/js"></script>

<script>
  function initialize() {
  var myLatlng = new google.maps.LatLng(41.380366, 2.178908);
  var mapOptions = {
    zoom: 16,
    center: myLatlng,
    scrollwheel: false
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'BCNLIP Language School'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>



@endsection
