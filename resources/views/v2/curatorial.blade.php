@extends('layouts.basev2')
@section('content')

<div class="mmac-container pv">
  <h1 class="mmac-page-title">{{ trans('curatorial.heading')}}</h1>
  {!! trans('curatorial.intro') !!}
</div>

@include('partials.curatorial.networks')



<div class="mmac-container pvt pvb">
  <h1 class="mmac-page-title">{{ trans('curatorial.international-exhibition.heading')}}</h1>
  {!! trans('curatorial.international-exhibition.text') !!}
</div>

<div class="mmac-container pvb">
  <h1 class="mmac-page-title">{{ trans('curatorial.annual-residence-competition.heading')}}</h1>
  {!! trans('curatorial.annual-residence-competition.text') !!}
</div>

<div class="mmac-container pvb">
  <h1 class="mmac-page-title">{{ trans('curatorial.talks.heading')}}</h1>
  {!! trans('curatorial.talks.text') !!}
</div>


@endsection
