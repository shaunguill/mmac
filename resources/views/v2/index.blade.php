@extends('layouts.basev2')
@section('title', 'Homepage')

@section('content')
  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
      <div class="uk-vertical-align uk-text-center uk-cover-background" style="background: url('images/common/covers/cover1.jpg') center center no-repeat; height: 450px;">
        <div class="uk-vertical-align-middle uk-width-1-2">
          <h1 class="uk-heading-large uk-text-contrast">Current Exhibition</h1>
          <p class="uk-text-large uk-text-contrast">Title of upcoming exhibition</p>
          <p>
            <a class="uk-button uk-button-primary uk-button-large" href="#">View this exhibition</a>
            <a class="uk-button uk-button-outline uk-button-large" href="#">View all exhibitions</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
      <div class="uk-grid">
        <div class="uk-width-1-6">
          <i class="uk-icon-cog uk-icon-large uk-text-primary"></i>
        </div>
        <div class="uk-width-5-6">
          <h2 class="uk-h3">Blog</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>

    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
      <div class="uk-grid">
        <div class="uk-width-1-6">
          <i class="uk-icon-thumbs-o-up uk-icon-large uk-text-primary"></i>
        </div>
        <div class="uk-width-5-6">
          <h2 class="uk-h3">Art Fairs & Events</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>

  </div>

  <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
      <div class="uk-grid">
        <div class="uk-width-1-6">
          <i class="uk-icon-dashboard uk-icon-large uk-text-primary"></i>
        </div>
        <div class="uk-width-5-6">
          <h2 class="uk-h3">International Network</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>

    <div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-1">
      <div class="uk-grid">
        <div class="uk-width-1-6">
          <i class="uk-icon-cloud-download uk-icon-large uk-text-primary"></i>
        </div>
        <div class="uk-width-5-6">
          <h2 class="uk-h3">Artist of the Season</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>

  </div>

  <hr class="uk-grid-divider">

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
      <div class="uk-panel uk-panel-box uk-text-center">
        <p><strong>Artist of the Season:</strong> Some Name Here <a class="uk-button uk-button-primary uk-margin-left" href="#">Read More</a></p>
      </div>
    </div>
  </div>

  <h1 class="uk-text-center"><a href="{{action('ExhibitionsController@index')}}">@lang('titles.archived-exhibitions')</a></h1>

  <div class="uk-grid uk-grid-small" data-uk-grid-margin>

    @foreach($latestexhibs as $exhibition)
    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-4">
      <figure class="uk-overlay uk-overlay-hover uk-width-1-1">
        <img src="
        @if($exhibition->galleries->count()>0 && $exhibition->galleries->first()->images->count()>0)
          {{asset('images/galleries/'.$exhibition->galleries->first()->images->first()->filename)}}
        @else
          {{asset('images/common/blank/exhibition.jpg')}}
        @endif
        " class="uk-width-1-1" alt="{{$exhibition->title}}">
       <figcaption class="uk-overlay-panel uk-overlay-background uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-text-center">
          <div>
            <h3 class="uk-margin-remove">{{$exhibition->title}}</h3>
            <h5 class="uk-margin-remove">{{$exhibition->startdate->format('j M Y')}} &ndash; {{$exhibition->enddate->format('j M Y')}}</h5>
          </div>
        </figcaption>
        <a class="uk-position-cover" href="{{action('ExhibitionsController@show', ['id'=>$exhibition->id])}}"></a>
      </figure>
    </div>
    @endforeach

  </div>
@endsection
