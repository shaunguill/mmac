@extends('layouts.basev2')
@section('content')

<div class="mmac-container pv">
  <h1 class="mmac-page-title">Support</h1>
  <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-2-3">
      <section id="our-support">
        <hr>
        <h2>{{ trans('support.our-support.heading') }}</h2>
        {!! trans('support.our-support.text') !!}
      </section>
    </div>

    <div class="uk-width-2-3 uk-push-1-3">
      <section id="donate" class="uk-margin-top">
        <hr>
        <h2>{{ trans('support.donate.heading') }}</h2>
        {!! trans('support.donate.text') !!}
      </section>
    </div>

    <div class="uk-width-2-3">
      <section id="corporate" class="uk-margin-top">
        <hr>
        <h2>{{ trans('support.corporate.heading') }}</h2>
        {!! trans('support.corporate.text') !!}
      </section>
    </div>

    <div class="uk-width-2-3 uk-push-1-3">
      <section id="foundation" class="uk-margin-top">
        <hr>
        <h2>{{ trans('support.foundation.heading') }}</h2>
        {!! trans('support.foundation.text') !!}
      </section>
    </div>

    <div class="uk-width-2-3">
      <section id="leaders-circle" class="uk-margin-top">
        <hr>
        <h2>{{ trans('support.leaders-circle.heading') }}</h2>
        {!! trans('support.leaders-circle.text') !!}
      </section>
    </div>

    <div class="uk-width-2-3 uk-push-1-3">
      <section id="current-friends" class="uk-margin-top">
        <hr>
        <h2>{{ trans('support.current-friends.heading') }}</h2>
        {!! trans('support.current-friends.text') !!}
      </section>
    </div>

  </div>
</div>

@endsection
