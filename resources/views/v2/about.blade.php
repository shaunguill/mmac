@extends('layouts.basev2')
@section('content')



  <h1>{{ trans('about.heading') }}</h1>

  <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-2-3 uk-margin-large-bottom">
      <section id="about">
        <p class="uk-text-bold">{!! trans('about.intro.text') !!}</p>
        <h2 class="uk-margin-large-top">{{ trans('about.background.heading') }}</h2>
        {!! trans('about.background.text') !!}
      </section>
    </div>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/about.jpg')}}" alt="" />
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/history-vision.jpg')}}" alt="" />
    </div>
    <div class="uk-width-2-3 uk-margin-large-bottom">
      <section id="history-and-vision" class="uk-margin-top">

        <h2>{{trans('about.history-vision.heading')}}</h2>
        {!! trans('about.history-vision.text') !!}
      </section>
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3">
      <section id="mission" class="uk-margin-top">
        <h2>{{ trans('about.mission.heading') }}</h2>
        {!! trans('about.mission.text') !!}
      </section>
    </div>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/mission.jpg')}}" alt="" />
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/founder.jpg')}}" alt="" />
    </div>
    <div class="uk-width-2-3">
      <section id="founder" class="uk-margin-top">
        <h2>The founder</h2>
        <p>
          {!! trans('about.founder.text') !!}
        </p>
      </section>
    </div>
  </div>


@endsection
