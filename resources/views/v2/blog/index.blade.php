@extends('layouts.basev2')
@section('content')


<h1 class="mmac-page-title">Blog</h1>

@foreach($articles as $article)
  <hr>
  <article class="uk-clearfix uk-margin-large-bottom">
    @foreach($article->galleries as $gallery)
      @include('partials.gallery')
    @endforeach
    <span class="uk-display-block uk-text-light">/ {{$article->created_at->format('m-d-y')}}</span>
    <h2>{{$article->title}}</h2>
    <p>
      {!!$article->body!!}
    </p>
  </article>
@endforeach


@endsection
