@extends('layouts.mmac-base')

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">Gallery</h1>

  @foreach($galleries as $gallery)

    <div>
      <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="{gutter:5}">
        @foreach($gallery->images as $image)
        <div>
          <script type="text/javascript" src="{{asset('js/components/lightbox.min.js')}}"></script>
          <link rel="stylesheet" href="{{asset('css/components/slidenav.min.css')}}" media="screen" title="no title" charset="utf-8">
          <a data-uk-lightbox="{group:'gallery'}" title="{{$image->title}}" href="{{asset('/images/galleries/'.$image->filename)}}">
            <img src="{{asset('/images/galleries/t/'.$image->filename)}}" alt="" />
          </a>
        </div>
        @endforeach
      </div>
    </div>


  @endforeach


</div>

@endsection
