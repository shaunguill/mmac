@extends('layouts.mmac-base')

@section('content')




      <div class="mmac-container pv">

        <h1 class="mmac-page-title">{{ trans('contact.heading') }}</h1>

        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

          <div class="uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-1">
            <!--<form class="uk-form" action="index.html" method="post">
              <div class="uk-form-row">
                <label class="uk-form-label" for="name">{{trans('contact.text.name')}}</label>
                <input type="text" class="uk-width-1-1" name="name">
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label" for="email">{{trans('contact.text.email')}}</label>
                <input type="email" class="uk-width-1-1" name="email">
              </div>
              <div class="uk-form-row">
                <label class="uk-form-label" for="message">{{trans('contact.text.message')}}</label>
                <textarea class="uk-width-1-1" name="message" rows="8"></textarea>
              </div>
              <div class="uk-form-row">
                <a class="uk-button uk-width-1-1" href="#" style="background-color:#323437;color:#fff;">{{trans('contact.text.send')}}</a>
              </div>
            </form>-->

            <h2>
              {{trans('contact.text.general-information')}}
            </h2>
            <p class="uk-text-large uk-margin-remove">
              {{trans('contact.text.mmac-gallery')}}
            </p>
            <p class="uk-text-large uk-margin-remove">
              Mamacuchara
            </p>
            <h4>
              {{trans('contact.text.space')}}
            </h4>
            <ul class="uk-list uk-list-space">
              <li>13 Carabassa</li>
              <li>Barcelona, {{trans('contact.text.spain')}} 08002</li>
              <li><i class="icon-mobile"></i> (0034) 654623602</li>
              <li><i class="icon-mail"></i> <a href="mailto:info@mmacgallery.com"> info@mmacgallery.com </a></li>
            </ul>
            <hr>
            <h2>{{trans('contact.text.connect-online')}}</h2>
            <ul class="uk-subnav">
              <li><a target="_blank" href="https://www.facebook.com/mmacss"><i class="icon-large icon-hover icon-facebook"></i></a></li>
              <li><a target="_blank" href="https://www.linkedin.com/in/mmac-mamacuchara-a6965898"><i class="icon-large icon-hover icon-linkedin"></i></a></li>
              <li><a target="_blank" href="https://twitter.com/MMACgallery"><i class="icon-large icon-hover icon-twitter"></i></a></li>
              <li><a target="_blank" href="https://www.instagram.com/mmacgallery/"><i class="icon-large icon-hover icon-instagram"></i></a></li>
              <li><a target="_blank" href="https://www.youtube.com/user/mmacgallery"><i class="icon-large icon-hover icon-youtube"></i></a></li>
              <li><a target="_blank" href="https://vimeo.com/gallerymmac"><i class="icon-large icon-hover icon-vimeo"></i></a></li>
            </ul>
          </div>
          <div class="uk-width-large-2-3 uk-width-medium-2-3 uk-width-small-1-1">
            <div id="map-canvas" style="width:100%; height:350px;top:0px;"></div>
          </div>
        </div>
      </div>


  <div class="uk-width-1-1 uk-flex uk-flex-middle uk-flex-center pv" style="background-image:url('../images/common/contact-hours.jpg');background-size:cover;background-position:right top">
    <div class="uk-width-2-3">

      <h1 class="uk-text-center">{{trans('contact.text.hours')}}</h1>

      <div class="uk-grid uk-grid-small">
        <div class="uk-width-1-2 uk-text-right">
          <ul class="uk-list uk-list-space">
            <li><span class="uk-text-bold">{{trans('contact.text.tue-fri')}}</span></li>
            <li><span class="uk-text-bold">{{trans('contact.text.sat')}}</span></li>
            <li><span class="uk-text-bold">{{trans('contact.text.holidays')}}</span></li>
            <li><span class="uk-text-bold">{{trans('contact.text.sunday')}}</span></li>
          </ul>
        </div>
        <div class="uk-width-1-2">
          <ul class="uk-list uk-list-space">
            <li> 16 am &ndash; 8.30 pm</li>
            <li> 14 am &ndash; 7 pm</li>
            <li> 14 am &ndash; 18 pm</li>
            <li> &mdash;</li>
          </ul>
        </div>
      </div>

    </div>
  </div>










<script src="https://maps.googleapis.com/maps/api/js"></script>

<script>
  function initialize() {
  var myLatlng = new google.maps.LatLng(41.380366, 2.178908);
  var mapOptions = {
    zoom: 16,
    center: myLatlng,
    scrollwheel: false
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'BCNLIP Language School'
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

@stop
