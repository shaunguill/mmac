@extends('layouts.mmac-base')

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">{{ trans('about.heading') }}</h1>
  <div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-2-3 uk-margin-bottom">
      <section id="about">
        {!! trans('about.intro.text') !!}
        <h2 class="uk-margin-large-top">{{ trans('about.background.heading') }}</h2>
        {!! trans('about.background.text') !!}
      </section>
    </div>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/about.jpg')}}" alt="" />
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/history-vision.jpg')}}" alt="" />
    </div>
    <div class="uk-width-2-3 uk-margin-bottom">
      <section id="history-and-vision" class="uk-margin-top">

        <h2>{{trans('about.history-vision.heading')}}</h2>
        {!! trans('about.history-vision.text') !!}
      </section>
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3">
      <section id="mission" class="uk-margin-top">
        <h2>{{ trans('about.mission.heading') }}</h2>
        {!! trans('about.mission.text') !!}
      </section>
    </div>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/mission.jpg')}}" alt="" />
    </div>
  </div>

    <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-3">
      <img src="{{asset('images/common/founder.jpg')}}" alt="" />
    </div>
    <div class="uk-width-2-3">
      <section id="founder" class="uk-margin-top">
        <h2>The founder</h2>
        <p>
          {!! trans('about.founder.text') !!}
        </p>
      </section>
    </div>
  </div>


  <!--
  <hr>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-2-3">
      <section id="staff" class="uk-margin-top">
        <h2>Staff</h2>
        <p>
          <mark>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</mark>
        </p>
      </section>
    </div>
  </div>-->

</div>









@stop
