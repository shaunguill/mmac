@extends('layouts.mmac-base')

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">
    <a href="{{action('ArtistsController@index')}}">{{trans('items.items')}}</a>
    <i class="icon-small icon-chevron-small-right"></i>&ldquo;{{$item->name}}&rdquo;
  </h1>

  <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
      @if($item->galleries->count()>0)
        <img src="{{asset('images/galleries/'.$item->galleries->first()->images->first()->filename)}}" class="uk-width-1-1" alt="{{$item->name}}" />
      @else
        <img src="{{asset('images/common/blank/item.jpg')}}" class="uk-width-1-1" alt="{{$item->name}}" />
      @endif
    </div>

    <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2">
      <h2>{{trans('items.description')}}</h2>
      <p>
        {{trans('items.artist')}}: <a href="{{action('ArtistsController@show', $item->artist->slug)}}"> {{$item->artist->name}} </a>
      </p>

      {!!$item->description!!}

      <h3>{{trans('items.price')}}</h3>
      <p>{{$item->price}} &euro;</p>
    </div>

  </div>


</div>

@endsection
