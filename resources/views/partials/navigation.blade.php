<div class="mmac-nav uk-width-1-1">

  <div class="mmac-container">
    <ul class="uk-subnav uk-flex uk-flex-right supnav">
      <li><a href="/blog"><i class="icon-message"></i> Blog</a></li>
      <li><a href="/shop"><i class="icon-cart"></i> {{trans('titles.shop')}}</a></li>
      <li data-uk-dropdown="{mode:'hover'}">
        <a href=""><i class="icon-globe"></i> {{LaravelLocalization::getCurrentLocale() }} </a>
        <div class="uk-dropdown ">
          <ul class="uk-nav uk-nav-dropdown">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
              <li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{{ $properties['native'] }}}</a></li>
            @endforeach
          </ul>
        </div>
      </li>
    </ul>
  </div>

  <div class="mmac-container">

    <nav class="uk-navbar">

      <a href="/" class="uk-navbar-brand">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="144px" height="44px" viewBox="0 0 144 44" style="enable-background:new 0.25 -4.013 125 70;" xml:space="preserve" >
          <path class="mmac-nav-logo" d="M95.707,3c-5.448,0-9.762,3.17-11.835,8.698c-1.306,3.482-1.344,6.851-1.344,7.223v21.939h9.903v-9.218h6.552
          	v9.218h9.902V18.92c0-0.372-0.037-3.74-1.343-7.223C105.469,6.17,101.155,3,95.707,3z M98.983,21.74h-6.552v-2.798
          	c0.003-0.131,0.055-2.121,0.775-3.925c0.843-2.114,1.801-2.114,2.5-2.114s1.658,0,2.501,2.114c0.719,1.804,0.772,3.794,0.775,3.925
          	V21.74z M78.852,10.161c1.178,2.827,1.239,5.509,1.239,6.019v24.68h-9.903V16.213c-0.01-0.372-0.124-1.483-0.536-2.381
          	c-0.39-0.849-0.686-0.929-1.368-0.929c-0.646,0-1.004,0.048-1.429,1.067c-0.426,1.021-0.476,2.198-0.476,2.209v24.68h-9.903V16.213
          	c-0.011-0.372-0.124-1.483-0.536-2.381c-0.39-0.849-0.685-0.929-1.368-0.929c-0.646,0-1.003,0.048-1.428,1.067
          	c-0.425,1.021-0.477,2.198-0.477,2.209v24.68h-9.902v-24.68c0-0.51,0.06-3.191,1.237-6.019C45.898,5.61,49.751,3,54.571,3
          	c0,0,0,0,0,0c2.626,0,4.964,0.776,6.855,2.217C63.319,3.776,65.656,3,68.283,3C73.103,3,76.957,5.61,78.852,10.161z M39.088,10.161
          	c1.178,2.827,1.238,5.509,1.238,6.019v24.68h-9.903V16.213c-0.009-0.372-0.123-1.483-0.537-2.381
          	c-0.389-0.849-0.685-0.929-1.367-0.929c-0.647,0-1.004,0.048-1.428,1.067c-0.426,1.021-0.476,2.198-0.476,2.209v24.68h-9.902V16.213
          	c-0.011-0.372-0.124-1.483-0.538-2.381c-0.39-0.849-0.684-0.929-1.368-0.929c-0.646,0-1.003,0.048-1.429,1.067
          	c-0.425,1.021-0.476,2.198-0.476,2.209v24.68H3v-24.68c0-0.51,0.059-3.191,1.238-6.019C6.134,5.61,9.987,3,14.807,3
          	c2.626,0,4.964,0.776,6.856,2.217C23.554,3.776,25.894,3,28.519,3C33.34,3,37.192,5.61,39.088,10.161z M130.519,21.663v-4.069
          	c-0.014-0.435-0.177-1.736-0.771-2.808c-0.515-0.931-1.377-1.883-3.875-1.883c-2.59,0-3.46,1.042-3.97,2.062
          	c-0.616,1.232-0.676,2.573-0.676,2.585v9.554c0.014,0.436,0.177,1.736,0.771,2.809c0.514,0.931,1.377,1.882,3.875,1.882
          	c2.589,0,3.459-1.041,3.97-2.061c0.616-1.232,0.676-2.573,0.676-2.585h9.903c0,0.596-0.083,3.734-1.722,7.013
          	c-2.43,4.86-6.986,7.537-12.827,7.537c-5.842,0-10.397-2.677-12.827-7.537c-1.641-3.279-1.722-6.417-1.722-7.013V17.55
          	c0-0.595,0.082-3.734,1.722-7.014C115.475,5.676,120.031,3,125.873,3c5.841,0,10.397,2.676,12.827,7.536
          	c1.639,3.28,1.722,6.419,1.722,7.014v4.113H130.519z"/>
        </svg>
      </a>

      <ul class="uk-navbar-nav uk-visible-large uk-navbar-flip">

        <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
          <a href="/services">{{trans('services.heading')}}</a>
          <div class="uk-dropdown uk-dropdown-navbar">
            <ul class="uk-nav uk-nav-navbar">
              <li><a href="/services#what-we-do">{{trans('services.what-we-do.heading')}}</a></li>
              <li><a href="/services#space-rental">{{trans('services.space-rental.heading')}}</a></li>
            </ul>
          </div>
        </li>

        <!-- EXPOSITION IMAGE PREVIEW IN DROPDOWN? -->
        <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
          <a href="/exhibitions">{{trans('exhibitions.heading')}}</a>
          <div class="uk-dropdown uk-dropdown-navbar">
            <ul class="uk-nav uk-nav-navbar">
              <li>
                @if($current)
                <a href="{{action('ExhibitionsController@show', ['id'=>$current->id])}}">
                {{trans('exhibitions.current.heading')}}:
                {{$current->title}}@else <a href="#">[No current exhibitions]</a> @endif
                </a>
              </li>
              <li>
                @if($upcoming)
                <a href="{{action('ExhibitionsController@show', ['id'=>$upcoming->id])}}">
                {{trans('exhibitions.upcoming.heading')}}:
                {{$upcoming->title}}@else <a href="#">[No upcoming exhibitions]</a> @endif
                </a>
              </li>
              <li class="uk-nav-divider"></li>
              <li><a href="/exhibitions/#archives">
                {{trans('exhibitions.past.heading')}}</a></li>
              <li><a href="/gallery">{{trans('exhibitions.gallery.heading')}}</a></li>
            </ul>
          </div>
        </li>

        <li><a href="/artists">{{trans('artists.heading')}}</a></li>
        <!-- ARTISTS IN SUBMENU? -->

        <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
          <a href="/curatorial-program">{{trans('curatorial.heading')}}</a>
          <div class="uk-dropdown uk-dropdown-navbar">
            <ul class="uk-nav uk-nav-navbar">
              <li><a href="/curatorial-program#international-network">{{trans('curatorial.international-network.heading')}}</a></li>
              <li><a href="/curatorial-program#international-exhibition">{{trans('curatorial.international-exhibition.heading')}}</a></li>
              <li><a href="/curatorial-program#annual-residence-competition">{{trans('curatorial.annual-residence-competition.heading')}}</a></li>
              <li><a href="/curatorial-program#talks">{{trans('curatorial.talks.heading')}}</a></li>
            </ul>
          </div>
        </li>

        <li>
          <a href="/art-fairs">{{trans('art-fairs-events.heading')}}</a>
        </li>

        <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
          <a href="/about">{{trans('about.heading')}}</a>
          <div class="uk-dropdown uk-dropdown-navbar">
            <ul class="uk-nav uk-nav-navbar">
              <li><a href="/about#background">{{trans('about.background.heading')}}</a></li>
              <li><a href="/about#history-vision">{{trans('about.history-vision.heading')}}</a></li>
              <li><a href="/about#mission">{{trans('about.mission.heading')}}</a></li>
              <li><a href="/about#founder">{{trans('about.founder.heading')}}</a></li>
              <li><a href="/about#staff">{{trans('about.staff.heading')}}</a></li>
              <li><a href="/about#press-room">{{trans('about.press-room.heading')}}</a></li>
            </ul>
          </div>
        </li>

        <li><a href="/contact">{{trans('contact.heading')}}</a></li>

        <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50, pos:'bottom-right'}">
          <a href="/support">{{trans('titles.support')}}</a>
          <div class="uk-dropdown uk-dropdown-navbar">
            <ul class="uk-nav uk-nav-navbar">
              <li><a href="/support#our-support">{{trans('support.our-support.heading')}}</a></li>
              <li><a href="/support#donate">{{trans('support.donate.heading')}}</a></li>
              <li><a href="/support#corporate">{{trans('support.corporate.heading')}}</a></li>
              <li><a href="/support#foundation">{{trans('support.foundation.heading')}}</a></li>
              <li><a href="/support#leaders-circle">{{trans('support.leaders-circle.heading')}}</a></li>
              <li><a href="/support#current-friends">{{trans('support.current-friends.heading')}}</a></li>
            </ul>
          </div>
        </li>

      </ul>

      <a href="#offcanvas" class="uk-navbar-toggle uk-navbar-flip uk-hidden-large" data-uk-offcanvas></a>

    </nav>
  </div>
</div>
