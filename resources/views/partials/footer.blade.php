<!-- footer -->
<footer>
  <div class="uk-grid">

    <div class="uk-width-1-1 pvt pvb__s">
      <div class="mmac-container">

        <div class="uk-grid uk-grid-medium" data-uk-grid-margin>


          <div class="uk-width-large-1-6 uk-visible-large">

            <ul class="uk-list">
              <li class="heading"><a href="services">Services</a></li>
              <li><a href="services#what-we-do">What we do</a></li>
              <li><a href="services#space-rental">Space rental</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="/exhibitions">Exhibitions</a></li>
              <li><a href="/exhibitions#current">Current</a></li>
              <li><a href="/exhibitions#upcoming">Upcoming</a></li>
              <li><a href="/exhibitions#archives">Archives</a></li>
            </ul>

          </div>

          <div class="uk-width-large-1-6 uk-visible-large">

            <ul class="uk-list">
              <li class="heading"><a href="/artists">Artists</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="/curatorial-program">Curatorial program</a></li>
              <li><a href="/curatorial-program#international-network">International network</a></li>
              <li><a href="/curatorial-program#international-exhibition">International exhibition</a></li>
              <li><a href="/curatorial-program#annual-residence-competition">Annual residence competition</a></li>
              <li><a href="/curatorial-program#talks">Talks</a></li>
            </ul>

          </div>

          <div class="uk-width-large-1-6 uk-visible-large">

            <ul class="uk-list">
              <li class="heading"><a href="/art-fairs">Art fairs &amp; Events</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="/about">About</a></li>
              <li><a href="/about#mission-and-vision">Mission &amp; vision</a></li>
              <li><a href="/about#biography">Biography</a></li>
              <li><a href="/about#founder">The founder</a></li>
              <li><a href="/about#staff">Staff</a></li>
              <li><a href="/about#press-room">Press room</a></li>
            </ul>

          </div>

          <div class="uk-width-large-1-6 uk-visible-large">

            <ul class="uk-list">
              <li class="heading"><a href="/contact">Contact</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="/support#our-support">Support</a></li>
              <li><a href="/support#our-support">Our Support</a></li>
              <li><a href="/support#donate">Donate</a></li>
              <li><a href="/support#corporate">Corporate</a></li>
              <li><a href="/support#foundation">Foundation</a></li>
              <li><a href="/support#leaders-circle">Leaders circle</a></li>
              <li><a href="/support#current-friends">Current friends</a></li>
            </ul>

          </div>

          <div class="uk-width-large-1-6 uk-visible-large">

            <ul class="uk-list">
              <li class="heading"><a href="/shop" data-uk-modal>Shop</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="/blog" data-uk-modal>Blog</a></li>
            </ul>

            <ul class="uk-list">
              <li class="heading"><a href="#newsletter-signup" data-uk-modal>Newsletter</a></li>
            </ul>

            <div class="uk-button-dropdown" data-uk-dropdown>
              <button class="uk-button"> <i class="icon-globe"></i> {{LaravelLocalization::getCurrentLocaleNative() }} </button>
              <div class="uk-dropdown">
                <ul class="uk-nav uk-nav-dropdown">
                  @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                  <li><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{{ $properties['native'] }}}</a></li>
                  @endforeach
                </ul>
              </div>
            </div>

          </div>

          <div class="uk-text-right uk-width-large-1-6 uk-width-medium-1-1 uk-width-small-1-1">

            <ul class="uk-list uk-list-space">
              <li><a target="_blank" href="https://www.facebook.com/mmacss"><i class="icon-large icon-facebook"></i></a></li>
              <li><a target="_blank" href="https://www.linkedin.com/in/mmac-mamacuchara-a6965898"><i class="icon-large icon-linkedin"></i></a></li>
              <li><a target="_blank" href="https://twitter.com/MMACgallery"><i class="icon-large icon-twitter"></i></a></li>
              <li><a target="_blank" href="https://www.instagram.com/mmacgallery/"><i class="icon-large icon-instagram"></i></a></li>
              <li><a target="_blank" href="https://www.youtube.com/user/mmacgallery"><i class="icon-large icon-youtube"></i></a></li>
              <li><a target="_blank" href="https://vimeo.com/gallerymmac"><i class="icon-large icon-vimeo"></i></a></li>
            </ul>

          </div>

        </div>
      </div>
    </div>

    <div class="uk-width-1-1 uk-text-center legal">
      <div class="mmac-container pv__s">
        © 2013 &mdash; {{Carbon\Carbon::now()->format('Y')}} MMAC (Mamacuchara) Contemporary Art Ltd.
        Reproduction in whole or in part is prohibited without the written permission of MMAC (Mamacuchara) Contemporary Art Ltd.
        <a href="#">Privacy Policy</a>
      </div>
    </div>

  </div>
</footer>


<div id="newsletter-signup" class="uk-modal">
  <div class="uk-modal-dialog">
    <a class="uk-modal-close uk-close"></a>
    <h1>MMAC Newsletter</h1>

    <form class="uk-form" action="index.html" method="post">
      <p>Fill in the form below and hit Send, and we'll keep you updated about everything MMAC.</p>
      <div class="uk-grid uk-grid-collapse">
        <div class="uk-width-2-3">
          <input class="uk-width-1-1" type="text" name="name" placeholder="your@email.com">
        </div>
        <div class="uk-width-1-3">
          <button class="uk-width-1-1 uk-button mmac-button-grey" type="submit" name="submit">Sign me up</button>
        </div>
      </div>
    </form>

  </div>
</div>
