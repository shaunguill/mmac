<div class="uk-width-1-1 bggxl">
  <div class="mmac-container pv">

    <h1 class="mmac-page-title">{{ trans('curatorial.international-network.heading')}}</h1>

    @if(count($networks) > 0)

      @foreach($networks as $network)
      <div class="uk-grid">
        <div class="@if($network->galleries->count()>0) uk-width-1-2 @else uk-width-2-3 @endif ">
          <h2 style="border-left: solid 4px #ff6599; padding-left: 0.5em;">{{$network->title}} </h2>
          <p>{!!$network->body!!}</p>
        </div>
        @if($network->galleries->count()>0)
        <div class="uk-width-1-2">
          @foreach($network->galleries as $gallery)
          <div class="uk-panel-box">
            <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-3" data-uk-grid="{gutter:2}">
              @foreach($gallery->images as $image)
              <div>
                <a data-uk-lightbox="{group:'gal-{{$gallery->id}}'}" title="{{$image->description}}" href="{{asset('images/galleries/'.$image->filename)}}">
                  <img class="uk-width-1-1" src="{{asset('images/galleries/t/'.$image->filename)}}" alt="" />
                </a>
              </div>
              @endforeach
            </div>
          </div>
          @endforeach
        </div>
        @endif
      </div>
      @endforeach

    @endif

  </div>
</div>
