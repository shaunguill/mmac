<div id="offcanvas" class="uk-offcanvas uk-text-center">
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-offcanvas uk-margin-large-top">
            <li>
                <h1>X</h1>
            </li>
            <li class="uk-active">
                <a href="/services">Services</a>
            </li>
            <li>
                <a href="/exhibitions">Exhibitions</a>
            </li>
            <li>
                <a href="/artists">Artists</a>
            </li>
            <li>
                <a href="/curatorial-program">Curatorial Program</a>
            </li>
            <li>
                <a href="/art-fairs">Art fairs</a>
            </li>
            <li>
                <a href="/about">About</a>
            </li>
            <li>
                 <a href="/support">Support</a>
            </li>
            <li>
                <a href="/shop">Shop</a>
            </li>
        </ul>
    </div>
</div>