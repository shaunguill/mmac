<div class="uk-width-large-1-2 uk-width-medium-1-2 uk-width-small-1-2 uk-panel-box uk-margin-bottom uk-float-right uk-margin-left">
  <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-3" data-uk-grid="{gutter:2}">
    @foreach($gallery->images as $image)
    <div>
      <a data-uk-lightbox="{group:'gal-{{$gallery->id}}'}" title="{{$image->description}}" href="{{asset('images/galleries/'.$image->filename)}}">
        <img class="uk-width-1-1" src="{{asset('images/galleries/t/'.$image->filename)}}" alt="" />
      </a>
    </div>
    @endforeach
  </div>
</div>
