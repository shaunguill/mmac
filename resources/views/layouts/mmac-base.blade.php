<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>MMAC GALLERY</title>

    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/uikit.css')}}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/mmac.css')}}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/components/slidenav.min.css')}}" media="screen" charset="utf-8">


    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/components/grid.min.js')}}"></script>
    <script src="{{asset('js/components/lightbox.min.js')}}"></script>



    @yield('addassets')

  </head>
  <body>

        @include('partials.navigation')

        @yield('content')

        @include('partials.footer')
        @include('partials.off-canvas')

  </body>

</html>
