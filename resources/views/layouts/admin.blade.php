<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="js-basepath" content="{{ asset('admin/') }}"/>
    <title>@yield('title', 'MMAC ADMIN')</title>
    <link href='https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Roboto:100,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('css/mmac-admin.css')}}" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/select2.min.css')}}" media="screen" title="no title" charset="utf-8">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/components/datepicker.min.css')}}" media="screen" title="no title" charset="utf-8">

    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/uikit.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/components/grid.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/components/datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.min.js')}}"></script>

    <script>
    $.ajaxSetup ({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      },
    });
    </script>

    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.0/summernote.js"></script>

    @yield('addassets')
  </head>
  <body>

  <div class="uk-width-1-1" style="background-color:#587094;min-height:60px;">
    <div class="uk-container uk-container-center">
      <nav class="uk-navbar">
        <a href="/admin" class="uk-navbar-brand" style="margin-right:20px;">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="125px" height="70px" viewBox="0 -4 125 70" style="enable-background:new 0.25 -4.013 125 70;" xml:space="preserve" >
            <style type="text/css">
            <![CDATA[
              .mmac-nav-logo{fill:#fff;}
            ]]>
            </style>
            <path class="mmac-nav-logo" d="M83.402,13.524c-4.688,0-8.402,2.729-10.188,7.486c-1.124,2.997-1.156,5.896-1.156,6.217v18.884h8.523v-7.934h5.641v7.934
            h8.522V27.228c0-0.319-0.032-3.22-1.156-6.217C91.807,16.253,88.093,13.524,83.402,13.524z M80.584,29.654v-2.407
            c0.002-0.113,0.047-1.826,0.667-3.379c0.726-1.819,1.55-1.819,2.151-1.819c0.603,0,1.428,0,2.154,1.819
            c0.617,1.553,0.664,3.266,0.666,3.379v2.407H80.584z"/>
            <path class="mmac-nav-logo" d="M59.799,13.524L59.799,13.524c-2.261,0-4.273,0.668-5.901,1.908c-1.628-1.24-3.641-1.908-5.901-1.908l0,0
            c-4.149,0-7.465,2.247-9.098,6.164c-1.014,2.435-1.065,4.742-1.065,5.181v21.243h8.523V24.868c0-0.011,0.044-1.022,0.41-1.902
            c0.366-0.877,0.673-0.918,1.229-0.918c0.588,0,0.842,0.069,1.177,0.801c0.355,0.772,0.453,1.729,0.461,2.05v21.214h8.524V24.868
            c0-0.011,0.043-1.022,0.41-1.902c0.365-0.877,0.673-0.918,1.229-0.918c0.588,0,0.842,0.069,1.178,0.801
            c0.355,0.772,0.453,1.729,0.461,2.05v21.214h8.524V24.868c0-0.438-0.051-2.747-1.065-5.181
            C67.264,15.771,63.947,13.524,59.799,13.524z"/>
            <path class="mmac-nav-logo" d="M25.572,13.524L25.572,13.524c-2.26,0-4.273,0.668-5.901,1.908c-1.628-1.24-3.641-1.908-5.901-1.908l0,0
            c-4.149,0-7.466,2.247-9.098,6.164c-1.014,2.435-1.065,4.742-1.065,5.181v21.243h8.523V24.868c0-0.011,0.044-1.022,0.41-1.902
            c0.366-0.877,0.673-0.918,1.229-0.918c0.588,0,0.842,0.069,1.177,0.801c0.356,0.772,0.453,1.729,0.462,2.05v21.214h8.523V24.868
            c0-0.011,0.043-1.022,0.41-1.902c0.365-0.877,0.672-0.918,1.229-0.918c0.588,0,0.842,0.069,1.177,0.801
            c0.355,0.772,0.454,1.729,0.462,2.05v21.214h8.524V24.868c0-0.438-0.051-2.747-1.065-5.181
            C33.037,15.771,29.722,13.524,25.572,13.524z"/>
            <path class="mmac-nav-logo" d="M121.893,29.588v-3.54c0-0.513-0.072-3.214-1.481-6.037c-2.093-4.184-6.015-6.486-11.041-6.486
            c-5.028,0-8.95,2.305-11.041,6.486c-1.412,2.822-1.481,5.524-1.481,6.037v8.263c0,0.513,0.07,3.214,1.481,6.036
            c2.091,4.184,6.013,6.487,11.041,6.487c5.026,0,8.948-2.305,11.041-6.487c1.409-2.822,1.481-5.523,1.481-6.036h-8.523
            c0,0.01-0.055,1.165-0.583,2.226c-0.438,0.878-1.188,1.773-3.417,1.773c-2.15,0-2.895-0.819-3.335-1.62
            c-0.513-0.923-0.651-2.042-0.664-2.417v-8.224c0-0.011,0.052-1.165,0.583-2.226c0.438-0.878,1.188-1.774,3.417-1.774
            c2.149,0,2.894,0.819,3.336,1.621c0.511,0.924,0.649,2.042,0.664,2.417v3.502L121.893,29.588L121.893,29.588z"/>
          </svg>
        </a>
        <ul class="uk-navbar-nav uk-visible-large">
          <li><a href="{{action('Admin\ExhibitionsController@index')}}">Exhibitions</a></li>
          <li><a href="{{action('Admin\ArtistsController@index')}}">Artists</a></li>
          <li><a href="{{action('Admin\FairController@index')}}">Fairs</a></li>
          <li><a href="{{action('Admin\ArticleController@index')}}">Blog Articles</a></li>
          <li><a href="{{action('Admin\GalleryController@index')}}">Gallery</a></li>
          <li><a href="{{action('Admin\ItemController@index')}}">Shop Items</a></li>
          <li><a href="{{action('Admin\NetworkController@index')}}">Networks</a></li>
          <li><a href="{{action('Admin\EventController@index')}}">Events</a></li>
          <li><a href="/logout"><i class="icon-circle-right"></i> Logout</a></li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="uk-container uk-container-center uk-margin-large-top uk-margin-large-bottom">
    @yield('content')
  </div>

  <script type="text/javascript">
    $('select').select2();
  </script>

  </body>
</html>
