<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MMAC Gallery &ndash; @yield('title')</title>
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="apple-touch-icon-precomposed" href="images/apple-touch-icon.png">

  <link rel="stylesheet" href="{{asset('css/uikit-v2.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/mmac-v2.css')}}"> <!--https://pagekit.com/marketplace/package/pagekit/theme-minimal-->

  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/uikit.min.js')}}"></script>
  <script src="{{asset('js/components/grid.min.js')}}"></script>
  <script src="{{asset('js/components/lightbox.min.js')}}"></script>

</head>
