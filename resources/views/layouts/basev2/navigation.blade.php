<nav class="uk-navbar uk-margin-large-bottom" style="background-color:transparent;" id="mmac-nav">

  <a class="uk-navbar-brand uk-visible-large" href="/">
    <img src="{{asset('images/common/mmac-logo.svg')}}" alt="MMAC Gallery" />
  </a>

  <ul class="uk-navbar-nav uk-visible-large">
    <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
      <a href="/services">@lang('services.heading')</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li><a href="/services#what-we-do">@lang('services.what-we-do.heading')</a></li>
          <li><a href="/services#space-rental">@lang('services.space-rental.heading')</a></li>
        </ul>
      </div>
    </li>
    <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
      <a href="/exhibitions">@lang('exhibitions.heading')</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li><a href="#">@lang('exhibitions.upcoming.heading')</a></li>
          <li><a href="#">@lang('exhibitions.current.heading')</a></li>
          <li><a href="/exhibitions/#archives">@lang('exhibitions.past.heading')</a></li>
          <li class="uk-nav-divider"></li>
          <li><a href="/gallery">@lang('exhibitions.gallery.heading')</a></li>
        </ul>
      </div>
    </li>
    <li><a href="/artists">@lang('artists.heading')</a></li>
    <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
      <a href="/curatorial-program">@lang('curatorial.heading')</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li><a href="/curatorial-program#international-network">@lang('curatorial.international-network.heading')</a></li>
          <li><a href="/curatorial-program#international-exhibition">@lang('curatorial.international-exhibition.heading')</a></li>
          <li><a href="/curatorial-program#annual-residence-competition">@lang('curatorial.annual-residence-competition.heading')</a></li>
          <li><a href="/curatorial-program#talks">@lang('curatorial.talks.heading')</a></li>
        </ul>
      </div>
    </li>
    <li>
      <a href="/art-fairs">@lang('art-fairs-events.heading')</a>
    </li>
    <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
      <a href="/about">@lang('about.heading')</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li><a href="/about#background">@lang('about.background.heading')</a></li>
          <li><a href="/about#history-vision">@lang('about.history-vision.heading')</a></li>
          <li><a href="/about#mission">@lang('about.mission.heading')</a></li>
          <li><a href="/about#founder">@lang('about.founder.heading')</a></li>
          <li><a href="/about#staff">@lang('about.staff.heading')</a></li>
          <li><a href="/about#press-room">@lang('about.press-room.heading')</a></li>
        </ul>
      </div>
    </li>
    <li><a href="/contact">@lang('contact.heading')</a></li>
    <li class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50, pos:'bottom-right'}">
      <a href="/support">@lang('titles.support')</a>
      <div class="uk-dropdown uk-dropdown-navbar">
        <ul class="uk-nav uk-nav-navbar">
          <li><a href="/support#our-support">@lang('support.our-support.heading')</a></li>
          <li><a href="/support#donate">@lang('support.donate.heading')</a></li>
          <li><a href="/support#corporate">@lang('support.corporate.heading')</a></li>
          <li><a href="/support#foundation">@lang('support.foundation.heading')</a></li>
          <li><a href="/support#leaders-circle">@lang('support.leaders-circle.heading')</a></li>
          <li><a href="/support#current-friends">@lang('support.current-friends.heading')</a></li>
        </ul>
      </div>
    </li>
    <li><a href="/blog"><i class="icon-message"></i> Blog</a></li>
    <li><a href="/shop"><i class="icon-cart"></i> @lang('titles.shop')</a></li>
  </ul>

  <style media="screen">
    #language-switch > a {
      color:#ff6599;
      font-size:0.9em;
      font-variant:small-caps;
      font-weight:700;
    }
    @media(max-width:959px){
      #language-switch > a {
        color:#000;
        font-size:1.2em;
      }
    }
  </style>

  <div class="uk-navbar-flip">
    <ul class="uk-navbar-nav">
      <li id="language-switch" class="uk-parent" data-uk-dropdown="{remaintime: 50, hoverDelayIdle: 50}">
        <a href="#modal-language" data-uk-modal> {{strtoupper(LaravelLocalization::getCurrentLocale()) }} </a>
        @include('layouts.basev2.lang-modal')
      </li>
    </ul>
  </div>

  <a href="#offcanvas" class="uk-navbar-toggle uk-hidden-large" data-uk-offcanvas></a>

  <div class="uk-navbar-brand uk-navbar-center uk-hidden-large">
    <img src="{{asset('images/common/mmac-logo.svg')}}" alt="MMAC Gallery" />
  </div>

</nav>
