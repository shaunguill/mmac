<div id="offcanvas" class="uk-offcanvas">
  <div class="uk-offcanvas-bar uk-flex uk-flex-middle uk-flex-center uk-text-right">
    <ul class="uk-nav uk-nav-offcanvas">
      <li><a href="/services">@lang('services.heading')</a></li>
      <li><a href="/exhibitions">@lang('exhibitions.heading')</a></li>
      <li><a href="/artists">@lang('artists.heading')</a></li>
      <li><a href="/curatorial-program">@lang('curatorial.heading')</a></li>
      <li><a href="/art-fairs">@lang('art-fairs-events.heading')</a></li>
      <li><a href="/about">@lang('about.heading')</a></li>
      <li><a href="/contact">@lang('contact.heading')</a></li>
      <li><a href="/support">@lang('titles.support')</a></li>
      <li><a href="/blog"><i class="icon-message"></i> Blog</a></li>
      <li><a href="/shop"><i class="icon-cart"></i> @lang('titles.shop')</a></li>
    </ul>
  </div>
</div>
