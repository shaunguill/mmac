<div id="modal-language" class="uk-modal">
  <div class="uk-modal-dialog uk-modal-dialog-blank">
    <button class="uk-modal-close uk-close" type="button"></button>
    <div class="uk-grid uk-flex-middle" data-uk-grid-margin>
      <div class="uk-width-1-1 uk-flex uk-flex-middle uk-flex-center uk-height-viewport uk-cover-background">
        <div class="uk-text-center">
          <h1 style="padding-bottom:5px;border-bottom:solid 1px #ff6599;">@lang('common.select-language'):</h1>
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
            <a class="uk-button uk-button-large uk-button-primary" rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
              {{{ $properties['native'] }}}
            </a>
            @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
