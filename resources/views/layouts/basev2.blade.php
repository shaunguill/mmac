<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
  @include('layouts.basev2.head')
  <body>
    <div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">

        @include('layouts.basev2.navigation')

        @yield('content')

    </div>
    @include('layouts.basev2.offcanvas')
    @include('layouts.basev2.footer')
  </body>
</html>
