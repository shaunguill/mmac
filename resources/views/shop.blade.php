@extends('layouts.mmac-base')

@section('addassets')
  <script type="text/javascript" src="{{asset('js/components/grid.min.js')}}"></script>
@stop

@section('content')

<div class="mmac-container pv">

  <h1 class="mmac-page-title">Online Shop</h1>

  <div id="item-control" class="uk-margin-bottom">
    <div class="uk-button-dropdown" data-uk-dropdown="{hoverDelayIdle: 50, remaintime: 50}">
      <button type="button" class="uk-button mmac-button-grey" name="button">Type <i class="icon-chevron-down"></i></button>
      <div class="uk-dropdown">
        <ul class="uk-nav uk-nav-dropdown">
          <li data-uk-filter=""><a href="">All</a></li>
          <li data-uk-filter="a"><a href="">Only A</a></li>
          <li data-uk-filter="b"><a href="">Only B</a></li>
        </ul>
      </div>
    </div>
    <div class="uk-button-dropdown" data-uk-dropdown="{hoverDelayIdle: 50, remaintime: 50}">
      <button type="button" class="uk-button mmac-button-grey" name="button">Sort by Price <i class="icon-chevron-down"></i></button>
      <div class="uk-dropdown">
        <ul class="uk-nav uk-nav-dropdown">
          <li data-uk-sort="item-price"><a href="">Ascending</a></li>
          <li data-uk-sort="item-price:desc"><a href="">Descending</a></li>
        </ul>
      </div>
    </div>
    <div class="uk-button-dropdown" data-uk-dropdown="{hoverDelayIdle: 50, remaintime: 50}">
      <button type="button" class="uk-button mmac-button-grey" name="button">Sort by Date <i class="icon-chevron-down"></i></button>
      <div class="uk-dropdown">
        <ul class="uk-nav uk-nav-dropdown">
          <li data-uk-sort="item-date"><a href="">Ascending</a></li>
          <li data-uk-sort="item-date:desc"><a href="">Descending</a></li>
        </ul>
      </div>
    </div>

  </div>


  @if(count($items) > 0)
  <div data-uk-grid="{controls:'#item-control',gutter:18}" class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4">
    @foreach($items as $item)

    <div data-item-price="{{$item->price}}" data-item-date="{{$item->created_at}}" data-uk-filter="a">
      <div>
        <figure class="uk-overlay">
          @if($item->galleries->count()>0)
          <img src="{{asset('images/galleries/t/'.$item->galleries()->first()->images->first()->filename)}}" width="" height="" alt="">
          @else
          <img src="{{asset('images/common/blank/item.jpg')}}" alt="" />
          @endif
          <figcaption class="uk-overlay-panel uk-flex uk-flex-bottom">
            <div>
              <h3 class="expotitle">{{$item->name}}</h3>
              <h4 class="expodate">{{$item->price}} &euro;</h4>
              <a class="uk-position-cover" href="{{action('ItemController@show', ['id'=>$item->id])}}"></a>
            </div>
          </figcaption>
        </figure>

      </div>
    </div>

    @endforeach
  </div>
  @else
  <p>
    <mark>No items found.</mark>
  </p>
  @endif

</div>

@stop
