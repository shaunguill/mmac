<?php

return [

    'services' => 'Services',

        'what-we-do' => 'What we do',
        'space-rental' => 'Space rental',

    'exhibitions' => 'Exhibitions',
    'artists' => 'Artists',
    'curatorial' => 'Curatorial program',
    'artfairs' => 'Art fairs &amp; Events',
    'about' => 'About',
    'support' => 'Support',
    'shop' => 'Shop',
    'items' => 'Items',

    'current' => 'Current',
    'upcoming' => 'Upcoming',
    'archived' => 'Archived',

    'current-exhibitions' => 'Current exhibitions',
    'upcoming-exhibitions' => 'Upcoming exhibitions',
    'archived-exhibitions' => 'Archived exhibitions',

    'images' => 'Images',

];
