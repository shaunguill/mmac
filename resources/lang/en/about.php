<?php

return [


    /* PAGE HEADER */

    "heading" => "About",

    /* ----- INTRO ------ */
    "intro" => [

        "text" =>
        "
          MMAC is an independent artists' Center founded in 2006. It focuses on the
          artistic development of talented emerging artists from within Barcelona and
          abroad.
        ",

    ],


    /* ----- BACKGROUND ------ */
    "background" => [

        "heading" => "Background",

        "text" =>
        "
        <p>
          MMAC, formerly known as &ldquo;Mamacuchara&rdquo;, was established in Poblenou in
          2006 by Giovanny Páez (Interdisciplinary visual artist). He was the initiator and had
          aimed to create a structure that would meet the need for beginning artists to have direct
          contacts with colleagues in a professional working environment.
        </p>
        <p>
          Currently located in the historic Gotic district of Barcelona, mamacuchara´s
          loft was born in 2003 when Giovanny happened upon a 200 square foot
          warehouse in the district of Poblenou  filled to the brim with objects culled by
          a local picker from garage sales, abandoned buildings, and flea markets. In
          2010 the space relocated to district of Gótic, where Giovanny Páez remains
          his Center Director and Curator. Currently working in partnership with BCN
          L.I.P international language School as logistical support.
        </p>
        <p>
          Like an artist in residence, Giovanny lived above his loft for 6 years. In 2004,
          he rented a home, and the loft evolved into its current incarnation as a
          creative hotspot.  The center features artists from around the world and
          embraces works of all mediums.
        </p>
        ",

    ],

    /* ----- HISTORY & VISION ------ */
    "history-vision" => [

        "heading" => "History &amp; Vision",

        "text" =>
        "
        <p>
          MMAC (space for contemporary art and thought) was founded by Giovanny
          in 2007 and started with an artists’ workshop in Upstate Barcelona in the
          summer of the same year. The workshop brought together a group of
          emerging and mid-career artists from Barcelona, Japan, South America and
          Germany, who spent weeks making work alongside each other. The
          workshop aimed to provide the artists with space and time to enter into
          dialogue and exchange ideas, knowledge and skills with each other. As
          such, the focus of the workshop was directed more towards the process of
          making work (the process) rather than the product. Today, the centre serves
          as an exhibition and work space in an international network.
        </p>
        ",

    ],

    /* ----- MISSION ------ */

    "mission" => [

        "heading" => "Mission",

        "text" =>
        "
        <p>
          Since its inception in the late 2006s, MMAC the center for contemporary art
          and thought has been active in collecting contemporary emerging art and
          supporting artists in the production of their work.
        </p>
        <p>
          The Center is dedicated to providing a platform for visual culture between
          Europe, Asia, Eastern countries and Latin America. Through research and
          publishing initiatives, support for exhibitions, conferences, educational
          seminars, the development of its collection, an artists' research initiative, and
          an emerging artist's prize, the Center will develop sustainable forms of
          cultural dialogue and debate within the region and beyond.
        </p>
        <p>
          With a regular activity in event production since 2006 with special emphasis
          on multidisciplinary activities understood as visual projects, theoretical and
          interdisciplinary creation.
        </p>
        <p>
          Acknowledging that artistic activity today is increasingly research-based and
          involved in the processes of knowledge production, the Center (MMAC)
          Projects initiative will commission six to ten artists per year to develop and
          document their research-based activities.
        </p>
        <p>
          The initiative works to engage the Internet, in partnership with one of the
          initiatives of the Centre, Format 0 (www.mmacgallery.com), an international
          network of artists, art centers, galleries, private companies, and new forms
          of patronage as generator space to create new artistic and curatorial
          projects, while also providing a platform to show processes of knowledge
          production and research involved in creative practices.
        </p>
        ",

    ],

    "founder" =>  [

        "heading" => "The Founder",

        "text" => "
        <p>
          CAVANNO Giovanny Páez is an Ecuadorean interdisciplinary artist, curator and producer of contemporary art projects in Europe and abroad. His work also engages different curatorial approaches to site specific performance, action and intervention in the public realm. His projects are often socially engaged and use a combination of participative and collaborative methodologies. Currently, Giovanny focuses her investigations on two areas: networking and Institutional critique; archiving - bio-art &ldquo;as a representation of power and social issues&rdquo;.
        </p>
        <p>
          In 2002, he has been invited as a visiting artist as well as a visiting lecturer at the Universities of Eastern, Western  and UK of Kentucky, EEUU.
        </p>
        <p>
          He has been creator and ideologist of &ldquo;Format 0&rdquo; (international exchange platform, reciprocity and collaboration on production, movement and residence for emerging artists). Founder of the Center for Arts and Contemporary Thought MMAC  which he is Curator and Director. (Space created for exchange network of exhibition projects.)
        </p>
        ",

    ],

    "staff" => [

        "heading"=>"Staff",

        "text"=>"...",

    ],

    "press-room" => [

        "heading"=>"Press Room",

        "text"=>"...",

    ],





];
