<?php


return [

    "heading" => "Exhibitions",

    /* --- PAGE EXHIBITION --- */

    "upcoming" => [

        "heading" => "Upcoming Exhibition",

        "text" => "...",

    ],

    /* --- PAGE EXHIBITION --- */

    "current" => [

        "heading" => "Current Exhibition",

        "text" => "...",

    ],

    /* --- PAGE EXHIBITION --- */

    "past" => [

        "heading" => "Past Exhibitions",

        "text" => "...",

    ],

    "gallery" => [

        "heading" => "Image Gallery",

        "text" => "...",

    ],

    "keywords" => [

        "view-all" => "View All",

        "archived" => "Archived",

    ],


];
