<?php


return [

    "heading" => "Artists",

    'artists' => 'Artists',
    'biography' => 'Biography',
    'exhibitions' => 'Exhibitions',
    'works' => 'Works',
    'website' => 'Website',
    'gallery' => 'Gallery',
    'download-cv' => 'Download CV',


];
