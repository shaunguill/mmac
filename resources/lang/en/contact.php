<?php


return [

    "heading" => "Contact",

    "text" => [

        "general-information" => "General information",

        "mmac-gallery" => "MMAC Gallery",

        "space" => "Space for arts and contemporary thought",

        "spain" => "Spain",

        "connect-online" => "Connect online",

        "hours" => "Gallery hours",

        "tue-fri" => "Tuesday to Friday",

        "sat" => "Saturday",

        "holidays" => "Holidays",

        "sunday" => "Closed on Sundays",



        "name" => "Name",

        "email" => "E-mail",

        "message" => "Message",

        "send" => "Send",

    ],


];
