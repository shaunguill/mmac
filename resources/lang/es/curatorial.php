<?php


return [

    "heading" => "Programa Curatorial",

    "intro" =>
      "
      ...
      ",

    /* ----- OUR SUPPORT ----- */

    "international-network" => [

        "heading" => "Red Internacional",

        "text" =>
        "
        ...
        ",

    ],

    /* ----- DONATE ----- */

    "international-exhibition" => [

        "heading" => "Exposiciones Internacionales",

        "text" =>
        "
        ...
        ",

    ],

    /* ----- ANNUAL RESIDENCE COMPETITION ----- */

    "annual-residence-competition" => [

        "heading" => "Concurso Anual de Residencia",

        "text" =>
        "
        ...
        ",

    ],

    /* ----- TALKS ----- */

    "talks" => [

        "heading" => "Charlas",

        "text" =>
        "
        ...
        ",

    ],

];
