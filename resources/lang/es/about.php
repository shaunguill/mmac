<?php

return [


    /* PAGE HEADER */

    "heading" => "Acerca",

    /* ----- INTRO ------ */
    "intro" => [

        "text" =>
        "
          MMAC es un Centro de artistas independientes fundada en 2006. Se centra
          en el desarrollo artístico de talentosos artistas emergentes desde el interior
          de Barcelona y en el extranjero.
        ",

    ],


    /* ----- BACKGROUND ------ */
    "background" => [

        "heading" => "Fondo",

        "text" =>
        "
        <p>
          MMAC, anteriormente conocido como &ldquo;Mamacuchara&rdquo;, se estableció en el
          Poblenou en 2006 por Giovanny Páez (interdisciplinario artista visual). Fue el
          iniciador y tenía como objetivo crear una estructura que responda a la
          necesidad de los artistas principiantes a tener contacto directo con los
          colegas en un ambiente de trabajo profesional.
        </p>
        <p>
          Actualmente se encuentra en el histórico barrio Gótico de Barcelona, ​​el loft de
          MMAC nació en 2003, cuando Giovanny se encontró con un almacén de 200
          metros cuadrados en el barrio de Poblenou lleno hasta el borde de los
          objetos extraídos por un recogerdor local desde las ventas de garaje, edificios
          abandonados, y mercados de pulgas. En 2010 el espacio se trasladó al
          distrito de Gótico, donde Giovanny Páez sigue siendo su Director y Curador
          del Centro. Actualmente se trabaja en colaboración con la escuela de idiomas
          internacionales BCN L.I.P como apoyo logístico.
        </p>
        <p>
          Como un artista en residencia, Giovanny vivía encima de su loft de 7 años.
          En 2004, alquiló una casa, y el loft se convirtió en su encarnación actual
          como un punto de acceso creativo. El centro cuenta con artistas de todo el
          mundo y abarca obras de todos los medios.
        </p>
        ",

    ],

    /* ----- HISTORY & VISION ------ */
    "history-vision" => [

        "heading" => "Historia &amp; Visión",

        "text" =>
        "
        <p>
          MMAC (espacio para el arte y el pensamiento contemporáneo) fue fundada
          por Giovanny Páez Cavanno en 2007 y comenzó como un taller de artistas
          en el norte de Barcelona, en el verano de ese mismo año. El taller trajo
          consigo a un grupo de artistas emergentes en la mitad de su carrera desde
          Barcelona, Japón, América del Sur y Alemania, quienes han pasado semanas
          haciendo trabajos juntos el uno al otro. El taller tuvo como objetivo
          proporcionar a los artistas con el espacio y el tiempo para entrar en diálogo e
          intercambio de ideas, conocimientos y habilidades entre sí. Como tal, el
          enfoque del taller fue dirigida más hacia el proceso de hacer el trabajo
          creador (el proceso) en lugar del producto. Hoy en día, el centro sirve como
          un espacio de trabajo y exposición en una red internacional.
        </p>
        ",

    ],

    /* ----- MISSION ------ */

    "mission" => [

        "heading" => "Misión",

        "text" =>
        "
        <p>
          Desde su creación a finales de 2006, MMAC Centro de arte y el pensamiento
          contemporáneo se ha mantenido activo en fomentar el arte emergente
          contemporánea y apoyar a los artistas en la producción de su trabajo.
        </p>
        <p>
          El Centro se dedica a proporcionar una plataforma para la cultura visual entre
          Europa, Asia, países del Este y América Latina. A través de las iniciativas de
          investigación y publicación, apoyo a exposiciones, conferencias, seminarios
          educativos, el desarrollo de su colección, iniciativa de investigación para
          artistas, y el premio de un artista emergente, el Centro desarrollará formas
          sostenibles de diálogo cultural y el debate dentro de la región y más allá.
        </p>
        <p>
          El centro ha estado llevando a cabo una actividad regular en la producción de
          eventos desde el año 2006, con especial énfasis en las actividades
          multidisciplinares entendidos como proyectos visuales, la creación teórica e
          interdisciplinario.
        </p>
        <p>
          Reconociendo el hecho de que la actividad artística hoy en día está cada vez
          más basado en la investigación y participa en los procesos de producción de
          conocimiento. La iniciativa de proyectos del centro pondrá en marcha seis a
          diez artistas por año para desarrollar y documentar sus actividades basadas
          en la investigación.
        </p>
        <p>
          La iniciativa trabaja para involucrar a Internet, en asociación con una de las
          iniciativas del Centro, &ldquo;Formato&rdquo; 0 (www.mmacgallery.com), una red
          internacional de artistas, centros de arte, galerías, empresas privadas, y las
          nuevas formas de patrocinio como una generador de espacios para crear
          nuevos proyectos artísticos y curatoriales, mientras que también proporciona
          una plataforma para mostrar los procesos de producción de conocimiento y la
          investigación que participan en las prácticas creativas.
        </p>
        ",

    ],

    /* ------ THE FOUNDER ----- */

    "founder" => [

        "heading" => "El Fundador",

        "text" => "
        <p>
          CAVANNO Giovanni Páez es un artista interdisciplinario ecuatoriano, curador y productor de proyectos de arte contemporáneo en Europa y en el extranjero. Su trabajo también involucra diferentes enfoques curatoriales a lugares en concreto, performance, acción e intervención en el ámbito público. Sus proyectos son a menudo socialmente comprometidos y utilizan una combinación de metodologías participativas y colaborativas. Actualmente, Giovanny centra sus investigaciones en dos áreas: trabajo en red y crítica institucional; archivo - bio-arte &ldquo;como una representación del poder y las cuestiones sociales&rdquo;.
        </p>
        <p>
          En el año 2002, ha sido invitado como artista visitante, así como profesor visitante en las Universidades de Eastern, del Western y UK de Kentucky, EEUU.
        </p>
        <p>
          Ha sido creador e ideólogo del &ldquo;Formato 0&rdquo; (plataforma de intercambio internacional, la reciprocidad y la colaboración en la producción, la circulación y de residencia para los artistas emergentes). Fundador del Centro de Arte y Pensamiento Contemporáneo MMAC que es curador y director. (Espacio creado para la red de intercambio de proyectos expositivos.)
        </p>
        ",

    ],

    "staff" => [

        "heading"=>"Personal",

        "text"=>"...",

    ],

    "press-room" => [

        "heading"=>"Press Room",

        "text"=>"...",

    ],


];
