<?php


return [

    "heading" => "Artistas",

    'artists' => 'Artistas',
    'biography' => 'Biografia',
    'exhibitions' => 'Exposiciones',
    'works' => 'Obras',
    'website' => 'Pagina web',
    'gallery' => 'Galeria',
    'download-cv' => 'Descargar CV',


];
