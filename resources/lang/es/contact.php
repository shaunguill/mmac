<?php


return [

    "heading" => "Contacta",

    "text" => [

        "general-information" => "Información general",

        "mmac-gallery" => "MMAC Galería",

        "space" => "Espacio para las Artes y el Pensamiento Contemporáneo",

        "spain" => "España",

        "connect-online" => "Conecta online",

        "hours" => "Horario de la Galería",

        "tue-fri" => "Martes a Viernes",

        "sat" => "Sábado",

        "holidays" => "Días Festivos",

        "sunday" => "Cerrado los Domingos",



        "name" => "Name",

        "email" => "E-mail",

        "message" => "Message",

        "send" => "Send",

    ],


];
