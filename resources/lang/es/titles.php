<?php

return [

    'services' => 'Servicios',

        'what-we-do' => 'Que hacemos',
        'space-rental' => 'Alquiler de espacio',

    'exhibitions' => 'Exposiciones',
    'artists' => 'Artistas',
    'curatorial' => 'Programa curatorial',
    'artfairs' => 'Art fairs',
    'about' => 'Acerca',
    'support' => 'Support',
    'shop' => 'Shop',
    'items' => 'Articulo',

    'current' => 'Actual',
    'upcoming' => 'Proxima',
    'archived' => 'Archivadas',

    'current-exhibitions' => 'Exposiciones actuales',
    'upcoming-exhibitions' => 'Proximas Exposiciones',
    'archived-exhibitions' => 'Exposiciones archivados',

    'images' => 'Imagenes',

];
