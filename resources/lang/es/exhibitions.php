<?php


return [

    "heading" => "Exposiciónes",

    /* --- PAGE EXHIBITION --- */

    "upcoming" => [

        "heading" => "Próxima Exposición",

        "text" => "...",

    ],

    /* --- PAGE EXHIBITION --- */

    "current" => [

        "heading" => "Exposición Actual",

        "text" => "...",

    ],

    /* --- PAGE EXHIBITION --- */

    "past" => [

        "heading" => "Exposiciónes Anteriores",

        "text" => "...",

    ],

    "gallery" => [

        "heading" => "Galería de Imagenes",

        "text" => "...",

    ],


    "keywords" => [

        "view-all" => "Ver todas",

        "archived" => "Archividas",

    ],

];
