<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    protected $fillable = [ 'filename', 'description' ];

    public function gallery() {

        return $this->belongsTo('App\Gallery');

    }
}
