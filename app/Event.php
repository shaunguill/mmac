<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = [ 'body' ];

    protected $dates = [ 'startdate', 'enddate', 'created_at', 'updated_at' ];

    protected $fillable = [

        'title',
        'startdate',
        'enddate',

    ];


    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }
    

}
