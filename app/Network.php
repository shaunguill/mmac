<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = [ 'title', 'body' ];

    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }

}
