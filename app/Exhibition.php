<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Jenssegers\Date\Date;
use Seiler\DateTrait\DateTrait;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Exhibition extends Model
{

    use DateTrait;

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update' => true,
    ];

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['description'];

    protected $dates = ['startdate', 'enddate', 'created_at', 'updated_at'];

    protected $fillable = [

        'title',
        'body',
        'price',
        'startdate',
        'enddate',
        'location',
        'openingtimes',

    ];

    // ELOQUENT RELATIONS TO OTHER MODELS

    public function artists()
    {
        return $this->belongsToMany('App\Artist');
    }

    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }

    /* MUTATORS */

    public function setStartdateAttribute($date)
    {
        $this->attributes['startdate'] = Carbon::createFromFormat('d-m-Y', $date);
    }

    public function setEnddateAttribute($date)
    {
        $this->attributes['enddate'] = Carbon::createFromFormat('d-m-Y', $date);
    }

    public function setOpeningtimesAttribute($string)
    {
        $this->attributes['openingtimes'] = str_replace('-', ' &ndash; ', $string);
    }

    public function getArtistListAttribute()
    {
        return $this->artists()->lists('id')->all();
    }

    public function getEnglishDescriptionAttribute()
    {
        return $this->translate('en')->description;
    }

    public function getSpanishDescriptionAttribute()
    {
        return $this->translate('es')->description;
    }



    /* SCOPES */

    /* upcoming */

    public function scopeUpcoming($query)
    {

        return $query->where('startdate', '>', Carbon::now()->format('Y-m-d'));

    }

    /* upcoming */

    public function scopeCurrent($query)
    {

        return $query->where('enddate', '>', Carbon::now()->format('Y-m-d'))
                      ->where('startdate', '<', Carbon::now()->format('Y-m-d'));

    }

    /* archived/past */

    public function scopeArchived($query)
    {

        return $query->where('enddate', '<', Carbon::now()->format('Y-m-d'))->orderBy('enddate', 'desc');

    }


}
