<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model implements SluggableInterface
{

    use SluggableTrait;
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['biography'];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'on_update' => true,
    ];

    protected $fillable = [
      'name',
      'biography',
      'website',
      'image',
    ];


    /* ARTIST IMAGE GALLERY */
    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }


    /* ARTIST EXHIBITIONS */
    public function exhibitions() {

        return $this->belongsToMany('App\Exhibition');

    }


    /* ARTIST ITEMS & WORKS */
    public function items() {

        return $this->hasMany('App\Item');

    }



    public function getExhibitionListAttribute()
    {

        return $this->exhibitions()->lists('id')->all();

    }


    public function getEnglishBioAttribute()

    {
        return $this->translate('en')->biography;

    }


    public function getSpanishBioAttribute()

    {
        return $this->translate('es')->biography;

    }

}
