<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = [ 'description' ];

    protected $fillable = [
        'name',
        'price',
        'description',
    ];


    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }


    public function artist() {

        return $this->belongsTo('App\Artist');

    }


}
