<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends Model implements SluggableInterface
{

    use SluggableTrait;

    protected $fillable = [ 'title' ];

    protected $sluggable = [ 'build_from' => 'title', 'save_to' => 'slug'];


    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }

}
