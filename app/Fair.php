<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fair extends Model
{

  use \Dimsav\Translatable\Translatable;

  protected $fillable = [ 'startdate', 'enddate' ];

  protected $dates = ['created_at', 'updated_at', 'startdate', 'enddate' ];

  public $translatedAttributes = [ 'title', 'description' ];


  public function galleries() {

      return $this->morphMany('App\Gallery', 'galleriable');

  }


}
