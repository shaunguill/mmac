<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Exhibition;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer(['partials.navigation', 'exhibitions.index'], function($view)
        {
            $view->with([
              'current' => Exhibition::current()->first(),
              'upcoming' => Exhibition::upcoming()->first(),
              'archived' => Exhibition::archived()->orderBy('enddate', 'desc')->get(),
            ]);
        });

        view()->composer('exhibitions.subnav', function($view)
        {
            $view->with([
              'upcoming' => Exhibition::upcoming()->get(),
              'current' => Exhibition::current()->get(),
              'archived' => Exhibition::archived()->orderBy('enddate', 'desc')->get(),
            ]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
