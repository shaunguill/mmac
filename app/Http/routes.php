<?php

Route::get('/', 'PagesController@index');

Route::get('en', 'PagesController@index');
Route::get('es', 'PagesController@index');


Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'web', 'localeSessionRedirect', 'localizationRedirect' ]
], function() {

    Route::get('services', 'PagesController@services');
    Route::get('curatorial-program', 'PagesController@curatorial');
    Route::get('art-fairs', 'PagesController@artfairs');
    Route::get('about', 'PagesController@about');
    Route::get('support', 'PagesController@support');
    Route::get('shop', 'ShopController@index');
    Route::get('blog', 'PagesController@blog');
    Route::get('contact', 'PagesController@contact');
    Route::get('gallery', 'PagesController@gallery');

    Route::get('exhibitions/archives', 'ExhibitionsController@archives');
    Route::resource('exhibitions', 'ExhibitionsController');
    Route::resource('artists', 'ArtistsController');
    Route::resource('items', 'ItemController');

});



/* ADMIN stuff */

Route::group([
    'prefix' => 'admin',
    'middleware' => [ 'web','auth', ]
], function () {

    Route::get('/', function () { return view('admin.index'); });
    Route::resource('exhibitions', 'Admin\ExhibitionsController');
    Route::resource('exhibitions/images', 'Admin\ImageController');
    Route::post('exhibitions/{id}/create-gallery', 'Admin\ExhibitionsController@createGallery');

    Route::get('gallery', 'Admin\GalleryController@index');
    Route::post('galleries/dropzoneupload', 'Admin\ImageController@dropzoneupload');
    Route::post('galleries/dropzonedelete', 'Admin\ImageController@dropzonedelete');

    Route::delete('gallery/{id}', 'Admin\GalleryController@destroy');

    Route::resource('artists', 'Admin\ArtistsController');
    Route::post('artists/{id}/create-gallery', 'Admin\ArtistsController@createGallery');

    Route::resource('fairs', 'Admin\FairController');
    Route::post('fairs/{id}/create-gallery', 'Admin\FairController@createGallery');

    Route::resource('items', 'Admin\ItemController');
    Route::post('items/{id}/create-gallery', 'Admin\ItemController@createGallery');

    Route::resource('articles', 'Admin\ArticleController');
    Route::post('articles/{id}/create-gallery', 'Admin\ArticleController@createGallery');
    Route::post('articles/ajaxupload', 'Admin\ArticleController@ajaxupload')->name('ajaxupload');

    Route::resource('networks', 'Admin\NetworkController');
    Route::post('networks/{id}/create-gallery', 'Admin\NetworkController@createGallery');

    Route::resource('events', 'Admin\EventController');


});

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('/home', 'HomeController@index');
});
