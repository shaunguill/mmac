<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use App\Fair;
use App\Article;
use App\Network;
use App\Event;
use App\Exhibition;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('index')->with([

          'latestexhibs' => Exhibition::archived()->limit(4)->get(),

        ]);

    }

    public function services()
    {

        return view('services');

    }

    public function curatorial(){


        return view('curatorial')->with('networks', $networks = Network::all());

    }



    /* English Index */

    public function english()
    {

        return 'English Index';

    }


    public function spanish()
    {

        return 'Spanish Index';

    }



    /* Art Fairs & Events */

    public function artfairs(){

        return view('art-fairs-events')->with([

            'fairs' => Fair::orderBy('enddate', 'desc')->get(),
            'events' => Event::orderBy('enddate', 'desc')->get(),

        ]);

    }



    /* Support page */

    public function support() {

        return view('support');

    }


    /* replace this function later with a dedicated ShopController class */
    public function shop() {

        return view('shop');

    }

    public function about()
    {

        return view('about');

    }

    public function exhibitions()
    {

        return view('exhibitions');

    }

    public function contact() {

        return view('contact');

    }

    public function gallery() {

        $page = Page::findBySlug('gallery');

        $galleries = $page->galleries;

        return view('gallery')->with('galleries', $galleries);

    }


    public function blog() {

        $articles = Article::orderBy('created_at', 'desc')->paginate(10);

        return view('blog')->with('articles',$articles);

    }



}
