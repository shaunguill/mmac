<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Exhibition;
use App\Artist;

class ExhibitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {



        $exhibitions = Exhibition::all();

        return view('exhibitions.index')->with([

          'current' => Exhibition::current()->get(),
          'upcoming' => Exhibition::upcoming()->get(),
          'archived' => Exhibition::archived()->get(),

        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $exhibition = Exhibition::findOrFail($id);

        return view('exhibitions.show')->with([
            'exhibition' => $exhibition,
        ]);
    }

    public function archives() {
        $exhibitions = Exhibition::archived()->get();
        return view('exhibitions.archives')->with('exhibitions', $exhibitions);
    }

}
