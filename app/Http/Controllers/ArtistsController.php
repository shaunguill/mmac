<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Artist;
use App\Exhibition;

class ArtistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $artists = Artist::All();
        return view('artists.index', compact('artists', $artists));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {

        $artist = Artist::findBySlugOrIdOrFail($slug);

        $all_artists = Artist::where('slug', '!=', $slug)->get();

        return view('artists.show')->with([
          'artist' => $artist,
          'all_artists' => $all_artists,
        ]);

    }

}
