<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Fair;
use App\Gallery;
use Carbon\Carbon;

class FairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $fairs = Fair::All();

        return view('admin.fairs.index')->with([
            'fairs' => $fairs,
         ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.fairs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

      $newfair = Fair::create([

          'startdate'=>Carbon::createFromFormat('d-m-Y', $request->input('startdate'))->format('Y-m-d'),
          'enddate'=>Carbon::createFromFormat('d-m-Y', $request->input('enddate'))->format('Y-m-d'),

      ]);

      foreach($request->input('locales') as $locale) {
          $newfair->translateOrNew($locale)->title = $request->input('title_'.$locale);
          $newfair->translateOrNew($locale)->description = $request->input('description_'.$locale);
      }

      $newfair->save();

      \Session::flash('status', 'Fair created successfully');

      return redirect()->action('Admin\FairController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $fair = Fair::findOrFail($id);
        return view('admin.fairs.edit')->with('fair', $fair);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $fair = Fair::findOrFail($id);

        $fair->update([

            'startdate'=>Carbon::createFromFormat('d-m-Y', $request->input('startdate'))->format('Y-m-d'),
            'enddate'=>Carbon::createFromFormat('d-m-Y', $request->input('enddate'))->format('Y-m-d'),

        ]);

        foreach($request->input('locales') as $locale) {
            $fair->translateOrNew($locale)->title = $request->input('title_'.$locale);
            $fair->translateOrNew($locale)->description = $request->input('description_'.$locale);
        }

        $fair->save();

        \Session::flash('status', 'Fair updated successfully');

        return redirect()->action('Admin\FairController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $fair = Fair::findOrFail($id);

        $fair->delete();

        \Session::flash('status', 'Fair deleted succesfully');

        return redirect()->action('Admin\FairController@index');

    }


    public function createGallery(Request $request, $id) {

        $fair = Fair::findOrFail($id);

        $newgallery = Gallery::create(['title'=>$request->input('title')]);

        $fair->galleries()->save($newgallery);

        \Session::flash('status', 'Gallery succesfully added');

        return redirect()->action('Admin\FairController@edit', $fair->id);

    }

}
