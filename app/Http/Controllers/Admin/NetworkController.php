<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Network;
use App\Gallery;

class NetworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.networks.index')->with('networks',Network::All());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.networks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $network = new Network;

        $network->save();

        foreach($request->input('locales') as $locale) {
            $network->translateOrNew($locale)->title  = $request->input('title_'.$locale);
            $network->translateOrNew($locale)->body   = $request->input('body_'.$locale);
        }

        $network->save();

        \Session::flash('status', 'Network added succesfully');

        return redirect()->action('Admin\NetworkController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('admin.networks.edit')->with('network', Network::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $network = Network::findOrFail($id);

        foreach($request->input('locales') as $locale) {
            $network->translateOrNew($locale)->title  = $request->input('title_'.$locale);
            $network->translateOrNew($locale)->body   = $request->input('body_'.$locale);
        }

        $network->save();

        \Session::flash('status', 'Network-item updated succesfully');

        return redirect()->action('Admin\NetworkController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createGallery(Request $request, $id) {

        $network = Network::findOrFail($id);

        $newgallery = Gallery::create(['title'=>$request->input('title')]);

        $network->galleries()->save($newgallery);

        \Session::flash('status', 'Gallery succesfully added');

        return redirect()->action('Admin\NetworkController@edit', $network->id);

    }


}
