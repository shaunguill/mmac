<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Exhibition;
use App\Artist;
use App\Gallery;

use Carbon\Carbon;

class ExhibitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $exhibitions = Exhibition::all();

        return view('admin.exhibitions.index')->with('exhibitions', $exhibitions);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        // retrieve all artists that can be associated with the exhibition
        $artists = Artist::lists('name', 'id');

        // render view with artists
        return view('admin.exhibitions.create')->with('artists',$artists);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['title'=>'required']);

        $newexhibition = Exhibition::create([

            'title' => $request->input('title'),
            'startdate' => $request->input('startdate'),
            'enddate' => $request->input('enddate'),
            'location' => $request->input('location'),
            'openingtimes' => $request->input('openingtimes'),
            'price' => $request->input('price'),

        ]);

        foreach($request->input('locales') as $locale) {
            $newexhibition->translateOrNew($locale)->description = $request->input('description_'.$locale);
        }

        $newexhibition->save();

        $newexhibition->artists()->attach($request->input('artists'));

        \Session::flash('status', 'Exhibition succesfully created, you can now add images <a class="uk-button uk-button=primary" href="#images">here</a>');

        return redirect()->action('Admin\ExhibitionsController@edit', $newexhibition->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $exhibition = Exhibition::findOrFail($id);

        return view('admin.exhibitions.show')->with([
            'exhibition' => $exhibition,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        // select exhibition to edit
        $exhibition = Exhibition::findOrFail($id);

        // get associated artists for exhibition
        $artists = Artist::lists('name', 'id');

        // render view with exhib info and artists associated
        return view('admin.exhibitions.edit')->with([
                    'exhibition' => $exhibition,
                    'artists' => $artists,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        // select exhib
        $exhibition = Exhibition::findOrFail($id);

        // validate input
        $this->validate($request, ['title'=>'required']);

        // update common exhib info
        $exhibition->update($request->all());

        foreach($request->input('locales') as $locale) {
            $exhibition->translateOrNew($locale)->description = $request->input('description_'.$locale);
        }

        $exhibition->save();

        // sync up artists associated with exhib
        $exhibition->artists()->sync($request->input('artist_list', []));

        return redirect()->action('Admin\ExhibitionsController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $exhibition = Exhibition::findOrFail($id);
        $exhibition->delete();
        return redirect()->action('Admin\ExhibitionsController@index');
    }


    public function createGallery(Request $request, $id) {


        $exhibition = Exhibition::findOrFail($id);

        $newgallery = Gallery::create(['title'=>$request->input('title')]);

        $exhibition->galleries()->save($newgallery);

        \Session::flash('status', 'Gallery succesfully added');

        return redirect()->action('Admin\ExhibitionsController@edit', $exhibition->id);


    }

}
