<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Article;
use App\Gallery;

class ArticleController extends Controller
{


    public function index() {

        $articles = Article::all();

        return view('admin.articles.index')->with('articles', $articles);

    }


      public function create() {

          return view('admin.articles.create');

      }


      public function store(Request $request) {

          /* $this->validate($request, [

              'title_en' => 'required_without:title_es|required_with:body_en',
              'body_en' => 'required_without:body_es|required_with:title_en',

              'title_es' => 'required_without:title_en|required_with:body_es',
              'body_es' => 'required_without:body_en|required_with:title_es',

          ]); */

          $article = new Article;

          $article->save();

          foreach($request->input('locales') as $locale) {
              $article->translateOrNew($locale)->title  = $request->input('title_'.$locale);
              $article->translateOrNew($locale)->body   = $request->input('body_'.$locale);
          }

          $article->save();

          \Session::flash('status', 'Article added succesfully');

          return redirect()->action('Admin\ArticleController@index');

      }

      public function edit($id) {

          $article = Article::findOrFail($id);

          return view('admin.articles.edit')->with('article', $article);

      }

      public function update(Request $request, $id) {

          /* $this->validate($request, [

              'title_en' => 'required_without:title_es|required_with:body_en',
              'body_en' => 'required_without:body_es|required_with:title_en',

              'title_es' => 'required_without:title_en|required_with:body_es',
              'body_es' => 'required_without:body_en|required_with:title_es',

          ]); */

          $article = Article::findOrFail($id);

          foreach($request->input('locales') as $locale) {
              $article->translateOrNew($locale)->title  = $request->input('title_'.$locale);
              $article->translateOrNew($locale)->body   = $request->input('body_'.$locale);
          }

          $article->save();

          \Session::flash('status', 'Article updated succesfully');

          return redirect()->action('Admin\ArticleController@index');

      }

      public function destroy($id)
      {

          $article = Article::findOrFail($id);

          $article->delete();

          return redirect()->action('Admin\ArticleController@index');

      }

      public function ajaxupload(Request $request) {

          $file = $request->file('file');
          $destinationPath = public_path().'/img/articles/';
          $filename = preg_replace('/\s+/', '_', $file->getClientOriginalName());
          $file->move($destinationPath, $filename);
          return '/img/articles/'.$filename;

      }



      public function createGallery(Request $request, $id) {

          $article = Article::findOrFail($id);

          $newgallery = Gallery::create(['title'=>$request->input('title')]);

          $article->galleries()->save($newgallery);

          \Session::flash('status', 'Gallery succesfully added');

          return redirect()->action('Admin\ArticleController@edit', $article->id);

      }

}
