<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Page;
use App\Gallery;
use App\Image;
use File;

class GalleryController extends Controller
{

    public function index(){

        $page = Page::findBySlug('gallery');

        return view('admin.gallery.index')->with('galleries', $page->galleries);

    }



    public function destroy($id) {

      $gallery = Gallery::findOrFail($id);

      foreach($gallery->images as $image) {
          File::delete(public_path('/images/galleries/'.$image->filename));
          File::delete(public_path('/images/galleries/'.$image->filename));
      }

      $gallery->delete();

      return 'success';

    }
}
