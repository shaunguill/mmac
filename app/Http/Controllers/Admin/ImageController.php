<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use File;
use App\Image;
use App\Gallery;
use Intervention\Image\ImageManagerStatic as Img;

class ImageController extends Controller
{

    public function dropzoneupload(Request $request) {

        $file = $request->file('file');
        $destinationPath        = public_path('images/galleries/');
        $destinationPathThumbs  = public_path('images/galleries/t/');

        $filename = str_random(24) . '.' . $file->getClientOriginalExtension();

        $file->move($destinationPath, $filename);

        $thumbnail = Img::make($destinationPath . $filename)->widen(300)->save($destinationPathThumbs . $filename);

        $gallery = Gallery::findOrFail($request->input('gallery_id'));

        $image = new Image;

        $image->filename = $filename;

        $gallery->images()->save($image);

        return $image;

    }



    public function dropzonedelete(Request $request) {

      $record = Image::findOrFail($request->get('id'));

      $file      = public_path('/images/galleries/') . $record->filename;
      $filethumb = public_path('/images/galleries/t/') . $record->filename;

      $record->delete();

      File::delete($file);
      File::delete($filethumb);

    }


}
