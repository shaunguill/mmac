<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;

use App\Item;
use App\Artist;
use App\Gallery;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Item::all();
        return view('admin.items.index')->with('items', $items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $artists = Artist::lists('name', 'id');

        return view('admin.items.create')->with('artists', $artists);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['name'=>'required', 'price'=>'required']);

        $newitem = Item::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
        ]);

        foreach($request->input('locales') as $locale) {
            $newitem->translateOrNew($locale)->description = $request->input('description_'.$locale);
        }

        if(!empty($request->input('artist'))) {
            $newitem->artist()->associate($request->input('artist'));
        }

        $newitem->save();

        \Session::flash('status', 'Item succesfully added');

        return redirect()->action('Admin\ItemController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);
        return view('admin.items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $item = Item::findorFail($id);

        $artists = Artist::lists('name', 'id');

        return view('admin.items.edit')
        ->with('item',$item)
        ->with('artists',$artists);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $item = Item::findOrFail($id);

        $this->validate($request, ['name'=>'required', 'price'=>'required']);

        $item->update($request->all());

        foreach($request->input('locales') as $locale) {
            $item->translateOrNew($locale)->description = $request->input('description_'.$locale);
        }

        if(!empty($request->input('artist'))) {
            $item->artist()->associate($request->input('artist'));
        } else {
            $item->artist()->dissociate();
        }

        $item->save();

        \Session::flash('status', 'Item updated successfully');
        return redirect()->action('Admin\ItemController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $item = Item::findOrFail($id);
      $item->delete();
      return redirect()->action('Admin\ItemController@index');
    }




    public function createGallery(Request $request, $id) {

        $item = Item::findOrFail($id);

        $newgallery = Gallery::create(['title'=>$request->input('title')]);

        $item->galleries()->save($newgallery);

        \Session::flash('status', 'Gallery succesfully added');

        return redirect()->action('Admin\ItemController@edit', $item->id);

    }


}
