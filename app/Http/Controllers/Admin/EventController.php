<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use Carbon\Carbon;

use File;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.events.index')->with('events', Event::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.events.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /* Basic information for Event (TITLE, START/ENDDATE) */

        $event = Event::create([

            'title' => $request->input('title'),
            'startdate' => Carbon::createFromFormat('d-m-Y', $request->input('startdate')),
            'enddate' => Carbon::createFromFormat('d-m-Y', $request->input('enddate')),

        ]);

        /* Image for Event */

        if($request->file('image')) {

            $imagepath = 'event_' . $event->id . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('image')->getClientOriginalExtension();
            $event->image = $imagepath;
            $request->file('image')->move(public_path('images/events/'), $imagepath);

        }

        /* Translations for Event */

        foreach($request->input('locales') as $locale) {

            $event->translateOrNew($locale)->body = $request->input('body_'.$locale);

        }

        $event->save();

        \Session::flash('status', 'Event <strong>' . $event->title . '</strong> successfully added');

        return redirect()->action('Admin\EventController@index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('admin.events.edit')->with('event', Event::findOrFail($id));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $event = Event::findOrFail($id);

        $event->update([

            'title' => $request->input('title'),
            'startdate' => Carbon::createFromFormat('d-m-Y', $request->input('startdate')),
            'enddate' => Carbon::createFromFormat('d-m-Y', $request->input('enddate')),

        ]);

        if($request->file('image_new')) {

            /* delete current image for artist */
            File::delete(public_path('images/events/'.$event->image));

            /* generate image name and set it as pathname in database, copy file to server */
            $newimagepath = 'event_' . $event->id . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('image_new')->getClientOriginalExtension();
            $event->image = $newimagepath;
            $request->file('image_new')->move(public_path('images/events/'), $newimagepath);

        }

        foreach($request->input('locales') as $locale) {

            $event->translateOrNew($locale)->body = $request->input('body_'.$locale);

        }

        $event->save();

        \Session::flash('status', 'Event <strong>' . $event->title . '</strong> successfully updated');

        return redirect()->action('Admin\EventController@index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $event = Event::findOrFail($id);

        File::delete(public_path('images/events/'.$event->image));

        $event->delete();

        \Session::flash('status', 'Event <strong>' . $event->title . '</strong> successfully deleted');

        return redirect()->action('Admin\EventController@index');


    }
}
