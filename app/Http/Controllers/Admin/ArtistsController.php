<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Admin\Controller;
use App\Artist;
use App\Exhibition;
use Carbon\Carbon;
use App\Gallery;

use File;

class ArtistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $artists = Artist::All();

        return view('admin.artists.index', compact('artists', $artists));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $exhibitions = Exhibition::lists('title', 'id');

        return view('admin.artists.create')->with('exhibitions', $exhibitions);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, ['name'=>'required']);

        $artist = Artist::create([
            'name' => $request->input('name'),
            'website' => $request->input('website'),
        ]);

        foreach($request->input('locales') as $locale) {
            $artist->translateOrNew($locale)->biography = $request->input('biography_'.$locale);
        }

        if($request->file('image')) {
            $imagepath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('image')->getClientOriginalExtension();
            $artist->image = $imagepath;
            $request->file('image')->move(public_path('images/artists/'), $imagepath);
        }

        if($request->file('cv')) {
            $cvpath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('cv')->getClientOriginalExtension();
            $artist->cv = $cvpath;
            $request->file('cv')->move(public_path('cv/'), $cvpath);
        }

        $artist->save();

        $artist->exhibitions()->attach($request->input('exhibitions'));

        \Session::flash('status', 'Artist <strong>' . $artist->name . '</strong> successfully added');

        return redirect()->action('Admin\ArtistsController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($slug)
    {

        $artist = Artist::findBySlugOrIdOrFail($slug);

        return view('admin.artists.show')->with([
          'artist' => $artist,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($slug)
    {

      $artist = Artist::findBySlugOrIdOrFail($slug);

      $exhibitions = Exhibition::lists('title', 'id');

      return view('admin.artists.edit')->with([
          'artist' => $artist,
          'exhibitions' => $exhibitions,
      ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $slug)
    {

        $this->validate($request, ['name'=>'required']);

        $artist = Artist::findBySlugOrIdOrFail($slug);
        $artist->name = $request->input('name');
        $artist->website = $request->input('website');

        if($request->file('image')) {

            /* generate image name and set it as pathname in database, copy file to server */
            $imagepath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('image')->getClientOriginalExtension();
            $artist->image = $imagepath;
            $request->file('image')->move(public_path('images/artists/'), $imagepath);

        }

        if($request->file('image_new')) {

            /* delete current image for artist */
            File::delete(public_path('images/artists/'.$artist->image));

            /* generate image name and set it as pathname in database, copy file to server */
            $newimagepath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('image_new')->getClientOriginalExtension();
            $artist->image = $newimagepath;
            $request->file('image_new')->move(public_path('images/artists/'), $newimagepath);

        }

        if($request->file('cv')) {

            /* generate filename and set it as pathname in the database, copy file to server */
            $cvpath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('cv')->getClientOriginalExtension();
            $artist->cv = $cvpath;
            $request->file('cv')->move(public_path('cv/'), $cvpath);

        }

        if($request->file('cv_new')) {

            /* delete old/current image (using path from database) */
            File::delete(public_path('cv/'.$artist->cv));

            /* generate new cv filepath, set it in database and upload file to server using filepath */
            $newcvpath = 'artist_' . $artist->slug . '_' . Carbon::now()->format('d-m-Y_H-i-s') . '.' . $request->file('cv_new')->getClientOriginalExtension();
            $artist->cv = $newcvpath;
            $request->file('cv_new')->move(public_path('cv/'), $newcvpath);

        }

        if($request->input('remove_cv'))
        {

            File::delete(public_path('cv/'.$artist->cv));
            $artist->cv = '';

        }

        foreach($request->input('locales') as $locale) {
            $artist->translateOrNew($locale)->biography = $request->input('biography_'.$locale);
        }

        $artist->save();

        $artist->exhibitions()->sync($request->input('exhibition_list', []));

        \Session::flash('status', 'Artist <strong>' . $artist->name . '</strong> succesfully updated');

        return redirect()->action('Admin\ArtistsController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $artist = Artist::findOrFail($id);

        File::delete(public_path('images/artists/'.$artist->image));
        File::delete(public_path('cv/'.$artist->cv));


        foreach($artist->galleries as $gallery) {

            foreach($gallery->images as $image) {

                File::delete(public_path('/images/galleries/' . $image->filename));
                File::delete(public_path('/images/galleries/t/' . $image->filename));

            }

        }

        $artist->galleries()->delete();

        $artist->delete();
        return redirect()->action('Admin\ArtistsController@index');
    }

    /* Galleries for Artist */

    public function createGallery(Request $request, $id) {

        $artist = Artist::findOrFail($id);

        $newgallery = Gallery::create(['title'=>$request->input('title')]);

        $artist->galleries()->save($newgallery);

        \Session::flash('status', 'Gallery for <strong>'.$artist->name.'</strong> succesfully added');

        return redirect()->action('Admin\ArtistsController@edit', $artist->id);

    }


}
