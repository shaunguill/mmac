<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistTranslation extends Model
{

    public $timestamps = false;
    
}
