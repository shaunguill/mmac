<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{

    protected $fillable = [ 'title' ];

    public function galleriable() {

        return $this->morphTo();

    }

    public function images() {

        return $this->hasMany('App\Image');

    }

}
