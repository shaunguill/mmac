<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FairTranslation extends Model
{
    public $timestamps = false;
}
