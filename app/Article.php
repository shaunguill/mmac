<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = [

        'title',
        'body',

    ];

    protected $fillable = [

        'title',
        'body',

    ];

    public function galleries() {

        return $this->morphMany('App\Gallery', 'galleriable');

    }

}
