<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('networks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('body');
            $table->timestamps();
        });

        Schema::create('network_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('network_id')->unsigned();
            $table->string('title');
            $table->text('body');
            $table->string('locale')->index();

            $table->unique(['network_id','locale']);
            $table->foreign('network_id')->references('id')->on('networks')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('network_translations');
        Schema::drop('networks');
    }
}
