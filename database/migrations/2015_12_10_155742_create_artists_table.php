<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('artists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('website');
            $table->string('cv');
            $table->string('image');
            $table->timestamps();
        });

        Schema::create('artist_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('artist_id')->unsigned();
            $table->text('biography');
            $table->string('locale')->index();

            $table->unique(['artist_id','locale']);
            $table->foreign('artist_id')->references('id')->on('artists')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('artist_translations');
        Schema::drop('artists');
    }
}
