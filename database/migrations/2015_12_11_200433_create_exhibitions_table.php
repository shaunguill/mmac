<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('imageurl');
            $table->date('startdate');
            $table->date('enddate');
            $table->string('location');
            $table->string('openingtimes');
            $table->tinyInteger('price');
            $table->timestamps();
        });

        Schema::create('exhibition_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('exhibition_id')->unsigned();
            $table->text('description');
            $table->string('locale')->index();

            $table->unique(['exhibition_id','locale']);
            $table->foreign('exhibition_id')->references('id')->on('exhibitions')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exhibition_translations');
        Schema::drop('exhibitions');
    }
}
