<?php

use Illuminate\Database\Seeder;

use App\Artist;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $a = Artist::create([
            'name' => 'Giovanny Páez',
            'image' => 'giovanny-paez.jpg',
        ]);
        $a->translateOrNew('en')->biography =
        '
        <p>
          Giovanny Páez is an Ecuadorean interdisciplinary Visual Artist and Curator based between Germany and Spain.
        </p>
        <p>
          His practice is concerned with the manipulation and appropriation of text, images and environments, through a variety of forms including print, installation, sculpture, sound and performance. His works explore the possibility of constructing a narrative whose aesthetics take sample from everyday objects based on autobiographical experiences while addressing the spectacle nature of the artwork itself, and so his work ranges from minimalist sculpture and media installation to photography, painting and performance video work.
        </p>
        <p>
          He has maintained his fascination with the human body and anatomy, which is a recurring theme in his work. He deals with biology, sexuality, history, mythology, sports and medicine, but especially in the construction of discourses about power and control.
        </p>
        ';
        $a->translateOrNew('es')->biography =
        '
        <p>
          Giovanny Páez (Quito, Ecuador) es un artista visual interdisciplinario y Curador residente entre Barcelona y Berlín.
        </p>
        <p>
          Su práctica se refiere a la manipulación y apropiación de texto, imágenes y entornos, a través de una variedad de formas, incluyendo impresión, instalación, escultura, sonido y performance. Sus obras exploran la posibilidad de construir una narrativa cuya estética toma muestras de objetos cotidianos basados ​​en experiencias autobiográficas al abordar la naturaleza espectáculo de la obra en sí, y por eso su trabajo abarca desde instalación minimalista de la escultura y los medios de comunicación a la fotografía, la pintura y el trabajo de performance.
        </p>
        <p>
          Mantiene su fascinación por el cuerpo y la anatomía, que es un tema recurrente en su obra. Él se ocupa de la biología, la sexualidad, la historia, la mitología, los deportes y la medicina, pero sobre todo por que en el se regula, construye y legisla formas de poder y el control.
        </p>
        ';
        $a->save();

        

        $a = Artist::create([
            'name' => 'Jaume Font',
            'image' => 'jaume-font.jpg',
        ]);
        $a->translateOrNew('en')->biography =
        '
        <p>In its installations critical social work, environment and territory. The manipulation of man in contemporany habitat. The ability to act naturally in appearance and concept by modifying the outcome. Sometimes with public involvement must manipulate and change the outcome work.</p>
        <p>Defend his sculptural works as versatile. The possibility of working parts, can adapt easily to various functions, either hung on the wall, ceiling hung, or installed on the floor. The textures of their parts are an important part of representation. Modifying the appearance of the structure. Sometimes a great visual weight and lightness rather than actual.</p>
        ';
        $a->translateOrNew('es')->biography =
        '
        <p>En sus instalaciones trabaja la crítica social, el entorno y el territorio. La manipulación del hombre en su habitat contemporáneo. La posibilidad de actuar en la naturaleza, modificando en aspecto y concepto el resultado final. A veces con la participación del público que debe manipular y cambiar el resultado del trabajo.</p>
        <p>Defiende sus obras escultóricas como versátiles. Trabajando la posibilidad de las piezas, capaces de adaptarse con facilidad a diversas funciones, ya sean colgadas en la pared, colgadas en el techo, o instaladas en el suelo. Las texturas de sus piezas son una parte importante de representación. Modificando el aspecto de la estructura. A veces de un gran peso visual y a la vez de una ligereza real.</p>
        ';
        $a->save();


    }
}
