<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(BaseSeeder::class);
        $this->call(FairSeeder::class);
        $this->call(ArtistSeeder::class);
        $this->call(NetworkSeeder::class);
        $this->call(ExhibitionSeeder::class);
        Model::reguard();
    }
}
