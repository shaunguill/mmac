<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class FairSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
        $fair = App\Fair::create([]);

        $fair->translateOrNew('en')->title = 'EN';
        $fair->translateOrNew('en')->description =
        '
        EN
        ';

        $fair->translateOrNew('es')->title = 'ES';
        $fair->translateOrNew('es')->description =
        '
        ES
        ';

        $fair->save();
        */

        /* BERLINER LIST 2013 */
        $startdate = Carbon::createFromFormat('d-m-Y', '19-09-2013');
        $enddate = Carbon::createFromFormat('d-m-Y', '22-09-2013');
        $fair = App\Fair::create(['startdate' => $startdate, 'enddate' => $enddate]);
        $fair->translateOrNew('en')->title = 'Berliner List 2013';
        $fair->translateOrNew('en')->description =
        '
        <p>
          Since September 18 MMAC Center in partnership with Sudacadreams is proud to present the solo exhibition entitled "Es tut mir leid" by  Ecuadorian artist Giovanny Paez (Cavanno) and as part of the 131 exhibitors from 30 countries listed in the International Art Fair Berliner list 2013.
        </p>

        <p>
          From September 19 to 22, BERLINER LISTE takes place during Berlin Art Week. From over 300 applicants who submitted their concepts before the end of July, curator Dr. Peter Funken has approved 131 exhibitors from 30 countries for the tenth anniversary of Berlin’s longest established art fair. The exhibition will be held in the Kraftwerk Berlin, whose extraordinary industrial architecture provides the exquisite setting to stage modern, exciting artwork. As the fair of discovery for modern and contemporary art, the BERLINER LISTE focuses particularly on working with emerging galleries and artists.
        </p>

        <p>
          Opening: Wednesday, September 18, 6pm in Kraftwerk Mitte and Tresor club<br/>
          First Choice for VIPs and press: Wednesday, September 18, 3pm
        </p>

        <h3>Fair location:</h3>
        <p>
          Kraftwerk Berlin<br/>
          Köpenicker Straße 70<br/>
          10179 Berlin Mitte<br/>
        </p>
        ';
        $fair->translateOrNew('es')->title = 'Berliner List 2013';
        $fair->translateOrNew('es')->description =
        '
        <p>
          Desde 18 de septiembre Center MMAC en colaboración con Sudacadreams se enorgullece en presentar la exposición individual titulada "es tut mir leid" por el artista ecuatoriano Giovanny Paez (Cavanno) y como parte de los 131 expositores de 30 países que figuran en la Feria Internacional de Arte Contemporáneo de Berlín lista de 2013.
        </p>

        <p>
          Del 19 al 22 de septiembre BERLINER LISTE tiene lugar durante la Semana de Arte de Berlín. De más de 300 aspirantes que presentaron sus conceptos antes de finales de julio, el Curador Dr. Peter Funken ha aprobado 131 expositores procedentes de 30 países para el décimo aniversario de la feria de arte de más arraigo en Berlín. La exposición se llevará a cabo en el Kraftwerk Berlin, cuya extraordinaria arquitectura industrial ofrece la exquisita opción para organizar , excitante obra, moderna. En la feria del descubrimiento para el arte moderno y contemporáneo, el BERLINER LISTE se centra especialmente en el trabajo de artistas y galerías emergentes.
        </p>

        <p>
          Apertura: Miércoles, Septiembre 18, 6pm in Kraftwerk Mitte and Tresor club<br/>
          Primera Opción para VIPs and prensa: Miércoles, Septiembre 18, 3pm
        </p>

        <h3>Ubicación Feria:</h3>
        <p>
          Kraftwerk Berlin<br/>
          Köpenicker Straße 70<br/>
          10179 Berlin Mitte<br/>
        </p>
        ';
        $fair->save();

        /* ACCESSIBLE ART FAIR */
        $startdate = Carbon::createFromFormat('d-m-Y', '16-10-2014');
        $enddate = Carbon::createFromFormat('d-m-Y', '19-10-2014');
        $fair = App\Fair::create(['startdate' => $startdate, 'enddate' => $enddate]);
        $fair->translateOrNew('en')->title = 'Accessible Art Fair';
        $fair->translateOrNew('en')->description =
        '
        <p>
          16/19 October 2014
        </p>
        <p>
          Each year, over fifty emerging and established artists from all over the world
          are selected by a professional jury in order to present their artworks to the
          public of potential buyers. Unlike traditional art fairs, the Accessible Art Fair is
          unique as it encourages interaction and dialogue between artists and buyers.
        </p>
        <p>
          Cercle de Lorraine – Club Van Lotharingen I 6 Place Poelaert &mdash; Poelaertplein,
          1000 BRUXELLES BRUSSEL – BELGIQUE BELGIË.
        </p>
        ';
        $fair->translateOrNew('es')->title = 'Accessible Art Fair';
        $fair->translateOrNew('es')->description =
        '
        <p>
          16/19 de octubre de 2014
        </p>
        <p>
          Cada año, más de cincuenta artistas emergentes y consolidados de todo el mundo
          son seleccionados por un jurado profesional con el fin de presentar sus obras de
          arte al público de potenciales compradores. A diferencia de las ferias de arte
          tradicionales, la Feria de Arte Accesible es única, ya que fomenta la interacción y el
          diálogo entre los artistas y los compradores.
        </p>
        <p>
          Cercle de Lorena - Lorena Van club I 6 Place Poelaert &mdash; Poelaertplein,
          1000 Bruxelles Brussel - BÉLGICA BÉLGICA.
        </p>
        ';
        $fair->save();

        /* WE ARE FAIR */
        $startdate = Carbon::createFromFormat('d-m-Y', '26-02-2016');
        $enddate = Carbon::createFromFormat('d-m-Y', '28-02-2016');
        $fair = App\Fair::create(['startdate' => $startdate, 'enddate' => $enddate]);
        $fair->translateOrNew('en')->title = 'We Are Fair!';
        $fair->translateOrNew('en')->description =
        '
        <p>
          Hotel Exe Central 4*<br>
          FEBRUARY 26th-28th 2016 / MADRID
        </p>

        <p>
          WE ARE FAIR! International Emerging Art Fair in Madrid aims to showcase
          new artistic tendencies: new exhibition areas, artists and projects that explore
          new ground. A space for creative exchange and promoting networking that
          has four sections.
        </p>

        <h3>HERE WE ARE!</h3>
        <p>
          A place in which the best exhibitors (galleries, institutions, etc) from the
          emerging art scene offer the most surprising pieces to a public that knows
          what is looking for. It is laid out in different rooms of the Hotel and will be an
          ad hoc curatorial feat specially for the occasion.
          If you represent a gallery, a cultural entity or an institution, HERE WE ARE! is
          your place. Transform one of the rooms in your exhibition area during this
          weekend. You will be able to show the work of 6 artists maximum or share the
          room with another entity
          MMAC Center in partnership with Sudacadreams is proud to present
        </p>

        <h3>ARTISTS</h3>
        <ul>
          <li>GARY ROSEMA</li>
          <li>CRISTINA BALLBÉ</li>
          <li>CAVANNO GIOVANNY PÁEZ</li>
          <li>JAUME FONT</li>
          <li>GERARD SABATE</li>
        </ul>
        ';

        $fair->translateOrNew('es')->title = 'We Are Fair!';
        $fair->translateOrNew('es')->description =
        '
        <p>
          Hotel Exe Central 4*<br>
          26/28 FEBRERO 2016 / MADRID
        </p>

        <p>
          WE ARE FAIR! Feria Internacional de Arte Emergente de Madrid, pretende
          poner en valor las nuevas tendencias artísticas: nuevos espacios expositivos,
          artistas y proyectos que exploren nuevos territorios. Un espacio de
          intercambio creativo y fomento del trabajo en red que cuenta con cuatro
          secciones.
        </p>

        <h3>HERE WE ARE!</h3>
        <p>
          Lugar en el que los mejores espacios del panorama artístico emergente
          ofrecen sus piezas más sorprendentes a un público que sabe lo que busca.
          Se sitúa en las diferentes habitaciones del Hotel, realizando una tarea de
          comisariado ad hoc para la ocasión.
        </p>

        <p>
          Si representas a una galería, colectivo artístico o una entidad o institución
          cultural, HERE WE ARE! es tu sitio. Convierte una de las habitaciones del
          hotel en tu espacio expositivo durante este fin de semana. Podrás mostrar al
          público la obra de hasta 6 artistas e incluso compartir el espacio con otra
          entidad.
        </p>

        <p>
          MMAC GALLERY en colaboración con SUDACADREAMS.CORP se complace en presentar
        </p>

        <h3>ARTISTAS</h3>
        <ul>
          <li>GARY ROSEMA</li>
          <li>CRISTINA BALLBÉ</li>
          <li>CAVANNO GIOVANNY PÁEZ</li>
          <li>JAUME FONT</li>
          <li>GERARD SABATE</li>
        </ul>
        ';

        $fair->save();

    }
}
