<?php

use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /* admin user */
        $admin = App\User::create([
          'name' => 'MMAC Admin',
          'email' => 'admin@mmac.com',
          'password' => bcrypt('mmacadmin'),
        ]);

        /* gallery for galleries page */

        $page = App\Page::create(['title'=>'Gallery']);
        $gallery = App\Gallery::create(['title'=>'Gallery']);

        $page->galleries()->save($gallery);


    }
}
