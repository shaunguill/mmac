<?php

use Illuminate\Database\Seeder;

use App\Network;

class NetworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $format0intro = new Network;
        $format0intro->translateOrNew('es')->title = 'Formato 0';
        $format0intro->translateOrNew('es')->body =
        '
        <p>
          Formato 0 inicialmente se concibió como un planteamiento de carácter técnico y teórico, en un primer momento se pensó sobre la posibilidad de trasladar la información creativa a formatos funcionales pudiendo ser estos entendidos como algo que va más allá de la obra artística como tal. Evidentemente existen distintos tipos de formatos dependiendo del tipo de creación o de la manera en que se planteen éstos, pero sobre todo condicionados por su modo de ser concebidos en cuanto trayectoria, profundidad, espacio, dimensión, viabilidad, estudio, desarrollo y contexto, y evidentemente sin perder de vista la importancia de la composición de la obra como un código en sí mismo, es decir, como una posibilidad de tipificar polisémicante un mensaje que conlleva también a niveles de interpretación múltiples. Sin embargo, el formato se refiere más a la creación que a la obra, al estilo o al uso estético que se haga del mismo, por ello también es importante tener en cuenta que en la creación intervienen este tipo de cuestiones, aunque no resulte determinante con respecto de factores de relevancia capital como serían la percepción, el sentido o la interpretación, impacto y sensibilidad, metáfora, lenguaje y lectura o simplemente su valor transgresor.
        </p>

        <p>
          En un inicio se pensó en el atractivo de establecer el proceso y la producción creativa como un código mutable de formato y que éste a su vez contendría varios formatos diferentes y los resultados de este proceso determinarían “la forma”. Aquí se asienta y enraíza la cuestión principal de este proyecto y es la siguiente: El formato hace que todo pueda ser codificable y esto puede comprobarse acudiendo a los distintos campos tanto del pensamiento y la creación: filosofía, ciencia, la historia, arte y técnica hacen que estas codificaciones puedan convertirse en formas concretas explicadas desde distintos sectores. Nuestra idea es hacer posible que un modelo, programa o diseño contenga contenidos creativos codificados desde distintos ámbitos dando origen a nuevas formas o la FORMA en sí.
        </p>

        <p>
          El formato es algo que se diseña y que puede ser rediseñado dependiendo de la forma y función que se le quiera dar a una determinada idea, objeto y realidad. El formato se consigue tras una búsqueda de modelos y experiencias paradigmaticas, asi mismo da lugar a una forma por la cual es entendido una experiencia del mundo o un posible sentido. De igual manera puede actuar como un contenedor para muchos tipos diferentes de lenguaje. A fin de cuentas es por su elasticidad y posibilidad por lo que ocupa un lugar privilegiado en nuestro proyecto.
        </p>';
        $format0intro->translateOrNew('en')->title = 'Format 0';
        $format0intro->translateOrNew('en')->body =
        '
        <p>
          Formato 0 Initially conceived as a theoretical and technical project, Formato 0 was thought at first as a possibility to translate creative information into functional formats, the latter being understood as transcending the artistic work per se.
        </p>
        <p>
          Evidently, there are different kinds of formats depending on the kind of creation or on the way that the work was proposed. The formats are specially conditioned by how they were conceived in terms of trajectory, depth, space, dimension, viability, study, development, and context. Evidently, one must not lose track the importance of the composition of the work as a code in and of itself. In other words, the composition as a code has the possibility of polysemantically typifying a message leading to different levels of interpretation. The format, however, refers more to the creation than to the work, to the style of to the aesthetic use of such work. Therefore, it is important to consider that in the creation process issues of the like intervene, although not always determining the process as much as other factors of capital relevance such as perception, sense or interpretation, impact and sensibility, metaphor, language and reading, or simply its transgressive value.
        </p>
        <p>
          At first, the attraction of a creative process and production as a mutable format code seemed interesting. Consequently, one can have different formats and the results of the process would determine “the form”. Here stands the main issue of this project, which is as follows:
          </p>
        <p>
          The format makes everything codifiable and this can be proved by accessing the different fields at the level of thought and at the moment of creation: philosophy, science, history, art, and techniques allow for these codifications to become concrete forms explained from different sectors.
        </p>
        <p>
          Our idea is to make possible a model, program or design containing creative contents codified from different fields, giving birth to new forms or to the FORM per se.
        </p>
        <p>
          The format is something that is designed and can be redesigned according to the form and function that one wants to give to an idea, object, and reality. The format is achieved through a quest for paradigmatic models and experiences. It also gives rise to a form through which an experience of the world, or a possible sense of it, is understood. Similarly, it can act as a container of multiple kinds of language. At the end, this elasticity and possibility allows it to occupy a privileged site in our project.
        </p>';
        $format0intro->save();


        $format0form1 = new Network;
        $format0form1->translateOrNew('en')->title = 'Format 0 &mdash; Form 1';
        $format0form1->translateOrNew('en')->body =
        '
        <p>
          Forma 1 Formato 0 Forma 1 was born in 2001 as a result of the dynamic and constitutive reflecting on the “format.” This epistemic justification mainly refers to the reasons by which we can consider appropriate of legitimate the fact of accepting new systems of symbolic creation and consumption. This theory was proposed from the first moment as an attempt to work beyond ideas of “genre” or “support,” and more in the realm of “format” from distinct realities, experiences, fields, and contexts. This derived into the borderline and regional displacements from foreign spaces into a local perception, giving birth to a deterritorialization and new metaphoric spaces. On the same note, we rethought the relationship between functionality, content, and decoding attitude that the format imposes upon the creator. In this sense, “forma 1” became a residual form of the format, resulting in the appropriation of functional objects. One of them was the “calendar 2001”, a project where each artist produced a piece with the format in mind, but whose content corresponded to a designated month, besides considering the enriching experience of the regional and cultural exchange with an invited country. In this case, it was the USA.
        </p>
        <p>
          Keeping relations of exchange and reciprocity with different countries lead to our “creative complaint”. It was a reaction to the internationalist and old fashion scholarship, support, residencies, and space systems for artists that surrounded centralized protocols and obsolete, bureaucratic profiles of admission. This affected both creation and management.
        </p>
        <p>
          It is worth mentioning and thanking to those who decisively collaborated in the execution of Forma 1 and made it possible for it to be participative, with an international impact. The significant role of this group remains in our memory by its implication and seriousness in the different shared responsibilities in the area of Logistics and Coordination in the USA directed by Anthropology Professor, Dr. Phyllis Passariello; Cultural Management through the US Embassy and specially its cultural representative from the Department of State, Susan Crystal; the Project co-director visual artist Diego Ponce Aritista Visual. We would also like to thank the DEPARTMENT OF STATE OF EEUU; AMERICAN AIRLINES; Centre College of Danville KY, EEUU; and Art Gallery “POSADA DE LAS ARTES KINGMAN” Quito, Ecuador.
        </p>
        <p>
          Without the action and complicity of people and institutions in “Forma 1” the ambition of this Project would have been nothing but another idea lost in its own management
        </p>';
        $format0form1->translateOrNew('es')->title = 'Formato 0 &mdash; Forma 1';
        $format0form1->translateOrNew('es')->body =
        '
        <p>
          Forma 1 nació en 2001 como consecuencia de una reflexión dinámica y constituyente sobre el “formato”. Esta justificación epistémica se refiere principalmente a las razones por las cuales puede considerarse apropiado o legítimo el hecho de aceptar nuevos sistemas de creación y consumo simbólico. Teoría que se planteó desde el primer momento como un intento de trabajar, más allá de la idea de “género” o “soporte”, la idea de “formato” desde distintas realidades, experiencias, ámbitos y contextos. Esto derivó también el traspaso fronterizo o regional de lugares ajenos a la propia percepción local, dando origen a una desterritorialización y a nuevos espacios metafóricos. Así mismo, se empezó hablar sobre la relación entre funcionalidad, el contenido y la actitud decodificadora que el formato impone al creador, de ésta manera “forma 1” se convirtió en una forma residual del “formato” dando como resultado la apropiación de objetos funcionales, uno de ellos sería “el calendario 2001” proyecto del cual cada artista produciría una obra pensando en el formato en cuestión, pero que su contenido estuviera en correspondencia con un mes designado, además, de considerar también la experiencia enriquecedora del intercambio regional y cultural con un país invitado. En este caso con EEUU.
        </p>
        <p>
          La idea de mantener relaciones de intercambio y reciprocidad con distintos paises en su momento se forjó como “queja creativa” al sistema internacionalista y anticuado de becas, soportes, residencias y espacios para artistas que se resuelven a través de protocolos centralizados y con perfiles de admisión obsoletos y burocráticos tanto en la creación como en la gestión.
        </p>
        <p>
          Es oportuno mencionar y extender agradecimientos a quienes en su momento colaboraron de manera descisiva para que la ejecución de Forma 1 fuera participativa y con impacto internacional. El protagonismo significativo de este grupo queda reseñado por su implicación y seriedad en distintas responsabilidades compartidas, así en la parte de Logística y Coordinación en EEUU. Dra. Phyllis Passariello (Profesora de Antropología); Gestión Cultural a través de la Embajada de los EEUU Susan Crystal (Agregada Cultural en el Departamento de Estado de los EEUU); Coodirector de Proyecto Diego Ponce Aritista Visual); de igual manera mis agradecimientos al DEPARTMENT OF STATE OF EEUU; AMERICAN AIRLINES (aerolíneas internacionales); Centre College of Danville KY, EEUU; Galería “POSADA DE LAS ARTES KINGMAN” Quito, Ecuador.
        </p>
        <p>
          Sin la actuación y complicidad tanto de personas e instituciones en “Forma 1” la audacia de este proyecto habría quedado una idea más que se pierde en su propia gestión.
        </p>
        ';
        $format0form1->save();


        $format0form2 = new Network;
        $format0form2->translateOrNew('es')->title = 'Formato 0 &mdash; Forma 2';
        $format0form2->translateOrNew('es')->body =
        '
        <p>
          La segunda forma que adopta el proyecto formato 0 nace desde la apreciación de la necesidad de dar un impulso real al intercambio transversal de ideas, recursos y creadores, en un contexto en el cual la sociedad moderna de la globalización presta una gran atención a las diferentes formas de cooperación internacional.
        </p>
        <p>
          La propia evolución del proyecto formato 0 desde su formación, y a través de sus diferentes fases, ha llevado a plantear la necesidad de una mayor generación de iniciativas a partir de procesos colaborativos en desarrollo, reflexión, reciprocidad y disponibilidad, para núcleo directivo.
        </p>
        <p>
          Para ello nace la idea de establecer una plataforma sólida de intercambio y flujo de recursos y capacidades para pensadores multidisciplinares que facilite y gestione elementos tales como residencia, espacios expositivos u otros, para facilitar la movilidad y el desarrollo de todos aquellos miembros que integran la red y de sus propuestas. Su noúmeno es por lo tanto la formación de una plataforma con carácter de red global e internacional de personas vinculadas al mundo del pensamiento y las artes contemporáneas, pensadores, creadores, comunicadores, mediadores y, por qué no, negociadores u otros, que comparten una plataforma libre y común sobre la que todos sus integrantes pueden pasar de la participación a la gestión, de la aceptación a la implementación de nuevos circuitos, de la ampliación a la disponibilidad y conformación de futuros eventos como acontecimientos. Este modelo toma prestado o se inspira en la idea del software libre (GNU, Linux) y del código abierto de su fundador y creador Richard Stallman.
        </p>
        <p>
          Este planteamiento tiene como finalidad congregar un número de invitados, representantes, instituciones, centros de Arte Contemporáneo, así como los propios artistas, pensadores, o actores implicados (toda la comunidad de profesionales, creadores emergentes...) en el proyecto, para que colaboren, generando así una serie de modelos y estrategias que sirvan como sustento y paradigma discursivo de un trabajo basado en la globalidad en red, fortaleciendo la plataforma colaborativa y su filosofía y proponiendo el diálogo, la reflexión y las inciciativas necesarias o dando apoyo e impulso entre todos a la producción de aquellos proyectos de creación que ya están en marcha.
        </p>
        <p>
          Es de importancia recordar que este proyecto surge de la necesidad de crear o formar espacios a través de un sistema de filiación, tanto de manera institucional como autogestionada e independiente en cualquier lugar del mundo, formando cada miembro parte de esta comunidad a potenciar desde todas las ramificaciones de la red, al mismo tiempo que la plataforma, ya como ente independiente, actúa como herramienta que garantiza el mejor funcionamiento de los proyectos que se propongan desde la comunidad que la integra.
        </p>
        ';
        $format0form2->translateOrNew('en')->title = 'Format 0 &mdash; Forma 2';
        $format0form2->translateOrNew('en')->body =
        '
        <p>
          The second form adopted by the Project formato 0 was born from the necessity of providing an impulse to a real, transversal exchange of ideas, resources, and creators in a context of a globalized, modern society where much attention is given to different forms of international cooperation.
        </p>
        <p>
          The evolution of formato 0 from its inception and through its different phases culminated in realizing there is a need for a larger initiative generator, arising from collaborative processes of development, reflection, reciprocity, and availability in order to encourage this kind of exchange as its directing center.
        </p>
        <p>
          For this purpose, the idea to establish a solid platform of exchange and resource and capacities flows was born. This would facilitate multidisciplinary thinkers’ management of residencies, exhibit spaces, the mobility and development of those members and their proposals that constitute the network. Its noumenon is therefore the formation of a platform with a global and international network character, thus integrating people linked with the world of thought and contemporary arts, thinkers, creators, communicators, mediators, and, why not, businessmen or others who share a free platform where all its members could go from the participation to the management, acceptance, and conformation of future events as facts. This model borrows from or is inspired by the free software idea of (GNU, Linux) or the open code, founded and created by Richard Stallman.
        </p>
        <p>
          This proposal has the objective of congregating a number of guests, representatives, institutions, contemporary art centers, as well as independent artists, thinkers or actors involved in the project (the community of professionals and emerging creators as a whole).This collaboration would generate a series of models and strategies that sustain and elaborate a discursive paradigm of a work based on the online globalization, thus strengthening a collaborative platform as well as its dialogical, reflective philosophy that proposes the needed initiative, support, and impulse between the members to produce and manage creative projects already on their way.
        </p>
        <p>
          It is important to remember that the project emerged from the need to create or form spaces through a filiation system in the institutional level as well as self-managing and independent people anywhere on the planet, each member standing for a community to be supported from any web ramification while the platform, as an independent space, acts as a tool that guarantees a better functioning of projects proposed from the integrated community.
        </p>
        ';
        $format0form2->save();


    }
}
