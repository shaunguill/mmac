<?php

use Illuminate\Database\Seeder;
use App\Exhibition;
use Carbon\Carbon;

class ExhibitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $e = new Exhibition;
      $e->title = 'The Poison is Turned Inside Out';
      $e->startdate = '31-07-2009';
      $e->enddate = '31-07-2009';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:30 - 02:30';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Exposicion. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'La Idea en La Materia';
      $e->startdate = '29-09-2010';
      $e->enddate = '29-09-2010';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '18:00 - 20:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Dau al Sis';
      $e->startdate = '18-12-2010';
      $e->enddate = '18-12-2010';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:00 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Imprescindible Comic';
      $e->startdate = '24-02-2011';
      $e->enddate = '24-02-2011';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '19:30 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Polvo Interior';
      $e->startdate = '07-04-2011';
      $e->enddate = '07-05-2011';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '19:30 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Espacios Compartidos, Tiempos Disociados';
      $e->startdate = '03-02-2012';
      $e->enddate = '03-02-2012';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:00 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'MMAC Gallery celebration';
      $e->startdate = '07-07-2012';
      $e->enddate = '07-07-2012';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:30 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'On Construction, Formato 0';
      $e->startdate = '28-06-2013';
      $e->enddate = '20-07-2013';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:30 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Swallow';
      $e->startdate = '01-11-2013';
      $e->enddate = '01-11-2013';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:00 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

      $e = new Exhibition;
      $e->title = 'Private Situations';
      $e->startdate = '15-03-2013';
      $e->enddate = '15-03-2013';
      $e->location = 'Somewhere over the rainbow';
      $e->openingtimes = '20:00 - 00:00';
      $e->price = '';
      $e->translateOrNew('en')->description = 'English description for Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->translateOrNew('es')->description = 'Descripcion en Espanyol para Past Exhibition One. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
      $e->save();

    }
}
