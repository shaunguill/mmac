$(document).ready(function() {

  Dropzone.options.dropzoneupload = {
    maxFilesize: 1,
    addRemoveLinks: true,
    acceptedFiles: 'image/*',

    init: function() {

      this.on('success', function (file, response) {
        file.serverFileID = response.id;
        $('#gallery_images').append('<div class="uk-overlay uk-overlay-hover" id="'+ response.id +'"><img src="/images/galleries/t/' + response.filename + '"><div class="uk-overlay-panel uk-flex uk-flex-center uk-flex-middle uk-text-center"><a href="#" class="delete-image uk-button uk-button-danger"><i class="icon-cross"></i></a></div></div>');
      });

      this.on('removedfile', function(file) {
        $('#'+file.serverFileID).remove();
        $.ajax({
          url: $('meta[name="js-basepath"]').attr('content') + '/galleries/dropzonedelete',
          type: "POST",
          data: {
            'id': file.serverFileID
          },
        });
      });
    },
  }

  $('.remove-gallery').on('click', function(e){
    var gid = $(this).data('galid');
    e.preventDefault();
    $.ajax({
      type: "DELETE",
      url: $('meta[name="js-basepath"]').attr('content') + '/gallery/' + gid,
      success: function() {
        alert('Deleted gallery with id = ' +gid);
        $('#gallery-'+gid).remove();
        $('<div class="uk-panel-box"><a class="uk-button uk-button-success" href="#create-gallery" data-uk-modal>Add Gallery <i class="icon-circle-with-plus"></i></a></div>')
        .insertAfter('#images');

      },
      error: function () {
        alert('Whoops, something went wrong with deleting Gallery with id = '+gid);
      },
    });
  });

  $('#gallery_images').on('click','.delete-image', function(e){
    e.preventDefault();
    $item = $(this).parent().parent();
    imgid = $item.attr('id');
    $.ajax({
      type: "POST",
      url: $('meta[name="js-basepath"]').attr('content') + '/galleries/dropzonedelete',
      data: {
        'id': imgid,
      },
      success: function() {
        $item.remove();
      },
      error: function () {
        alert('Whoops, something went wrong with deleting image with id = '+imgid);
      },
    });
  });

});
